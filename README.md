# DirectX Shader Showcase #
This project was an assignment to show the use of shaders in DirectX using HLSL. The project features the following techniques:

- Diffuse Mapping
- Wiggle Diffuse
- Diffuse Specular
- Normal Mapping
- Parallax Mapping
- Cell Shading
- Cutout and Additive Blending
- Multi Shadow Maps
- Portals

Platform: Windows  
Languages/Technology: DirectX 10, HLSL, C++  
Work Period: 6 Weeks  
Type: University Project  

## For more info or to request access, contact tom@tom-royle.co.uk ##