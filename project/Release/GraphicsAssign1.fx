//--------------------------------------------------------------------------------------
// File: GraphicsAssign1.fx
//
//	Shaders Graphics Assignment
//	Add further models using different shader techniques
//	See assignment specification for details
//--------------------------------------------------------------------------------------


//--------------------------------------------------------------------------------------
// Structures
//--------------------------------------------------------------------------------------

// Standard input geometry data, more complex techniques (e.g. normal mapping) may need more
struct VS_BASIC_INPUT
{
    float3 Pos    : POSITION;
    float3 Normal : NORMAL;
	float2 UV     : TEXCOORD0;
};

struct VS_NORMAL_INPUT
{
	float3 Pos     : POSITION;
    float3 Normal  : NORMAL;
    float3 Tangent : TANGENT;
	float2 UV      : TEXCOORD0;
};

struct VS_REFLECTION_INPUT
{
	float3 Pos		: POSITION;
	float3 Normal	: NORMAL;
};

// Data output from vertex shader to pixel shader for simple techniques. Again different techniques have different requirements
struct VS_BASIC_OUTPUT
{
    float4 ProjPos			: SV_POSITION;
    float2 UV				: TEXCOORD0;
};

struct VS_LIGHTING_OUTPUT
{
	float4 ProjPos       : SV_POSITION;
	float4 WorldPos		 : POSITION;
	float4 WorldNormal	 : NORMAL;
	float2 UV            : TEXCOORD0;
};

struct VS_NORMAL_OUTPUT
{
	float4 ProjPos      : SV_POSITION;
	float3 WorldPos     : POSITION;
	float3 ModelNormal  : NORMAL;
	float3 ModelTangent : TANGENT;
	float2 UV           : TEXCOORD0;
};

struct VS_TEXTURE_OUTPUT
{
	float4 ProjPos : SV_POSITION;
    float2 UV      : TEXCOORD0;
};

struct VS_PROJECTION_OUTPUT
{
    float4 ProjPos : SV_POSITION;
};

struct VS_CUBEMAP_OUTPUT
{
	float4 ProjPos : SV_POSITION;
    float3 UV      : TEXCOORD0;
};

struct VS_MIRROR_LIGHTING_OUTPUT
{
	float4 ProjPos     : SV_POSITION;
	float3 WorldPos    : POSITION;
	float3 WorldNormal : NORMAL;
    float2 UV          : TEXCOORD0;
	float  ClipDist    : SV_ClipDistance;
};

struct VS_MIRROR_BASIC_OUTPUT
{
	float4 ProjPos : SV_POSITION;
    float2 UV      : TEXCOORD0;
	float ClipDist : SV_ClipDistance;
};

struct PS_DEPTH_OUTPUT
{
    float4 Colour : SV_Target;
    float  Depth  : SV_Depth;
};


//--------------------------------------------------------------------------------------
// Global Variables
//--------------------------------------------------------------------------------------
// All these variables are created & manipulated in the C++ code and passed into the shader here

// The matrices (4x4 matrix of floats) for transforming from 3D model to 2D projection (used in vertex shader)
float4x4 WorldMatrix;
float4x4 ViewMatrix;
float4x4 ProjMatrix;
float4x4 WorldInverseTranspose;

// A single colour for an entire model - used for light models and the intial basic shader
float3 ModelColour;

// Diffuse texture map (the main texture colour) - may contain specular map in alpha channel
Texture2D DiffuseMap;
Texture2D NormalMap;
Texture2D CellMap;
Texture2D ShadowMap1;
Texture2D ShadowMap2;
Texture2D ShadowMap3;

TextureCube CubeMapTexture;

float ParallaxDepth;

float3	Tint;
float	Amount;

float3   light1Pos;
float3   light1Facing;
float4x4 light1ViewMatrix;
float4x4 light1ProjMatrix;
float    light1CosHalfAngle;
float3   light1Colour;

float3   light2Pos;
float3   light2Facing;
float4x4 light2ViewMatrix;
float4x4 light2ProjMatrix;
float    light2CosHalfAngle;
float3   light2Colour;

float3   light4Pos;
float3   light4Facing;
float4x4 light4ViewMatrix;
float4x4 light4ProjMatrix;
float    light4CosHalfAngle;
float3   light4Colour;

float3	 light3Pos;
float3	 light3Colour;

float3	ambientColour;
float	specularPower;
float3	cameraPos;
float	attenuation;
float	outlineThickness;
float4	ClipPlane;

SamplerState Trilinear
{
    Filter = MIN_MAG_MIP_LINEAR;
    AddressU = Wrap;
    AddressV = Wrap;
};

SamplerState PointSampleClamp
{
    Filter = MIN_MAG_MIP_POINT;
    AddressU = Clamp;
    AddressV = Clamp;
};

SamplerState Reflect
{
	Filter = MIN_MAG_MIP_LINEAR;
	AddressU = Mirror; 
	AddressV = Mirror; 
};


//--------------------------------------------------------------------------------------
// Vertex Shaders
//--------------------------------------------------------------------------------------

// Basic vertex shader to transform 3D model vertices to 2D and pass UVs to the pixel shader
//
VS_BASIC_OUTPUT BasicTransformVer( VS_BASIC_INPUT vIn )
{
	VS_BASIC_OUTPUT vOut;
	
	float4 worldPos = mul( float4(vIn.Pos, 1.0f), WorldMatrix );
	float4 viewPos  = mul( worldPos, ViewMatrix );
	vOut.ProjPos    = mul( viewPos,  ProjMatrix );
	
	// Pass texture coordinates (UVs) on to the pixel shader
	vOut.UV = vIn.UV;

	return vOut;
}
VS_BASIC_OUTPUT WiggleTextureVer(VS_BASIC_INPUT vIn)
{
	VS_BASIC_OUTPUT vOut;
	
	// Use matrices to transform the geometry to 2D
	float4 worldPos = mul( float4(vIn.Pos, 1.0f), WorldMatrix );
	float4 viewPos  = mul( worldPos, ViewMatrix );
	vOut.ProjPos    = mul( viewPos,  ProjMatrix );

	float SinY = sin(vIn.UV.y * radians(360.0f) + Amount*4);
	float SinX = sin(vIn.UV.x * radians(360.0f) + Amount*4);

	vIn.UV.x += 0.05f * SinY;
	vIn.UV.y += 0.05f * SinX;

	vOut.UV = vIn.UV;

	return vOut;
}
VS_LIGHTING_OUTPUT DiffuseSpecularVer(VS_BASIC_INPUT vIn)
{
	VS_LIGHTING_OUTPUT vOut;
	
	// Use matrices to transform the geometry to 2D
	float4 worldPos = mul( float4(vIn.Pos, 1.0f), WorldMatrix );

	float4 viewPos  = mul( worldPos, ViewMatrix );
	vOut.ProjPos    = mul( viewPos,  ProjMatrix );

	float4 worldNormal = mul(float4(vIn.Normal, 0.0f), WorldMatrix);
	worldNormal = normalize(worldNormal);
	
	vOut.WorldNormal = worldNormal;
	vOut.WorldPos = worldPos;

	vOut.UV = vIn.UV;

	return vOut;
}
VS_NORMAL_OUTPUT NormalMapVer(VS_NORMAL_INPUT vIn)
{
	VS_NORMAL_OUTPUT vOut;
	
	float4 worldPos = mul( float4(vIn.Pos, 1.0f), WorldMatrix );
	vOut.WorldPos = worldPos.xyz;

	float4 viewPos = mul( worldPos, ViewMatrix );
	vOut.ProjPos   = mul( viewPos,  ProjMatrix );

	vOut.ModelNormal  = vIn.Normal;
	vOut.ModelTangent = vIn.Tangent;

	vOut.UV = vIn.UV;

	return vOut;
}
VS_BASIC_OUTPUT OutlineVer(VS_BASIC_INPUT vIn)
{
	VS_BASIC_OUTPUT vOut;

	float4 worldPos = mul( float4(vIn.Pos, 1.0f), WorldMatrix );

	float4 viewPos  = mul( worldPos, ViewMatrix );

	float4 worldNormal = mul(float4(vIn.Normal, 0.0f), WorldMatrix);
	worldNormal = normalize(worldNormal);

	worldPos += outlineThickness * sqrt(viewPos.z) * worldNormal;

	viewPos			= mul(worldPos, ViewMatrix);
	vOut.ProjPos    = mul(viewPos, ProjMatrix);

	return vOut;
}
VS_TEXTURE_OUTPUT BasicTextureVer(VS_BASIC_INPUT vIn)
{
	VS_TEXTURE_OUTPUT vOut;
	
	float4 worldPos = mul( float4(vIn.Pos, 1.0f), WorldMatrix );
	float4 viewPos  = mul( worldPos, ViewMatrix );
	vOut.ProjPos    = mul( viewPos,  ProjMatrix );
	
	// Pass texture coordinates (UVs) on to the pixel shader
	vOut.UV = vIn.UV;

	return vOut;
}
VS_CUBEMAP_OUTPUT CubeMapVer (VS_BASIC_INPUT vIn)
{
	VS_CUBEMAP_OUTPUT vOut;

	float4 worldPos = mul( float4(vIn.Pos, 1.0f), WorldMatrix );
	float4 viewPos  = mul( worldPos, ViewMatrix );
	vOut.ProjPos    = mul( viewPos,  ProjMatrix ).xyww;

	vOut.UV = vIn.Pos;

	return vOut;
}
VS_MIRROR_LIGHTING_OUTPUT MirrorLightingVer(VS_BASIC_INPUT vIn)
{
	VS_MIRROR_LIGHTING_OUTPUT vOut;
	
	float4 worldPos = mul( float4(vIn.Pos, 1.0f), WorldMatrix );

	float4 viewPos  = mul( worldPos, ViewMatrix );
	vOut.ProjPos    = mul( viewPos,  ProjMatrix );

	float4 worldNormal = mul(float4(vIn.Normal, 0.0f), WorldMatrix);
	worldNormal = normalize(worldNormal);
	
	vOut.WorldNormal = worldNormal;
	vOut.WorldPos = worldPos;

	vOut.UV = vIn.UV;

	vOut.ClipDist = dot(worldPos, ClipPlane);

	return vOut;
}
VS_MIRROR_BASIC_OUTPUT MirrorVer(VS_BASIC_INPUT vIn)
{
	VS_MIRROR_BASIC_OUTPUT vOut;
	
	float4 worldPos = mul( float4(vIn.Pos, 1.0f), WorldMatrix );

	float4 viewPos  = mul( worldPos, ViewMatrix );
	vOut.ProjPos    = mul( viewPos,  ProjMatrix );

	vOut.UV = vIn.UV;

	vOut.ClipDist = dot(worldPos, ClipPlane);

	return vOut;
}

//--------------------------------------------------------------------------------------
// Pixel Shaders
//--------------------------------------------------------------------------------------


// A pixel shader that just outputs a single fixed colour
//
float4 OneColourPix( VS_BASIC_OUTPUT vOut ) : SV_Target
{
	return float4( ModelColour, 1.0 ); // Set alpha channel to 1.0 (opaque)
}

float4 TexturedPix(VS_BASIC_OUTPUT vOut) : SV_Target
{
	float3 TexColour = DiffuseMap.Sample( Trilinear, vOut.UV );
	float4 colour;
    colour.r = TexColour.r;
	colour.g = TexColour.g;
	colour.b = TexColour.b;
	colour.a = 1.0f;
	return colour;
}

float4 ScrollTexturePix(VS_BASIC_OUTPUT vOut) : SV_Target
{
	float3 TexColour = DiffuseMap.Sample( Trilinear, vOut.UV + Amount/16 );
	float3 Tint = {1.0, 0.0, 0.0};
	float4 colour;
    colour.r = TexColour.r + Tint.r;
	colour.g = TexColour.g + Tint.g;
	colour.b = TexColour.b + Tint.b;
	colour.a = 1.0f;
	return colour;
}

float4 DiffuseSpecularPix( VS_LIGHTING_OUTPUT vOut ) : SV_Target 
{
	float3 worldNormal = normalize(vOut.WorldNormal); 

	float3 CameraDir = normalize(cameraPos - vOut.WorldPos.xyz);

	/*---|Light 1|----------------------------------*/
	float3 Light1Dir = normalize(light1Pos - vOut.WorldPos.xyz);
	float3 Light1Dist = length(light1Pos - vOut.WorldPos.xyz);
	float3 DiffuseLight1 = (light1Colour * max( dot(worldNormal.xyz, Light1Dir), 0 )) / Light1Dist*attenuation;
	float3 halfway = normalize(Light1Dir + CameraDir);
	float3 SpecularLight1 = DiffuseLight1 * pow( max( dot(worldNormal.xyz, halfway), 0 ), specularPower );

	/*---|Light 2|-----------------------------------*/
	float3 Light2Dir = normalize(light2Pos - vOut.WorldPos.xyz);
	float3 Light2Dist = length(light2Pos - vOut.WorldPos.xyz);
	float3 DiffuseLight2 = (light2Colour * max( dot(worldNormal.xyz, Light2Dir), 0 )) / Light2Dist*attenuation;
	halfway = normalize(Light2Dir + CameraDir);
	float3 SpecularLight2 = DiffuseLight2 * pow( max( dot(worldNormal.xyz, halfway), 0 ), specularPower );

	/*---|Light 3|-----------------------------------*/
	float3 Light3Dir = normalize(light3Pos - vOut.WorldPos.xyz);
	float3 Light3Dist = length(light3Pos - vOut.WorldPos.xyz);
	float3 DiffuseLight3 = (light3Colour * max( dot(worldNormal.xyz, Light3Dir), 0 )) / Light3Dist*attenuation;
	halfway = normalize(Light3Dir + CameraDir);
	float3 SpecularLight3 = DiffuseLight3 * pow( max( dot(worldNormal.xyz, halfway), 0 ), specularPower );

	/*---|Light 4|-----------------------------------*/
	float3 Light4Dir = normalize(light4Pos - vOut.WorldPos.xyz);
	float3 Light4Dist = length(light4Pos - vOut.WorldPos.xyz);
	float3 DiffuseLight4 = (light4Colour * max( dot(worldNormal.xyz, Light4Dir), 0 )) / Light4Dist*attenuation;
	halfway = normalize(Light2Dir + CameraDir);
	float3 SpecularLight4 = DiffuseLight4 * pow( max( dot(worldNormal.xyz, halfway), 0 ), specularPower );

	/*---|Sum of Lighting|---------------------------*/
	float3 DiffuseLight = ambientColour + DiffuseLight1 + DiffuseLight2 + DiffuseLight3 + DiffuseLight4;
	float3 SpecularLight = SpecularLight1 + SpecularLight2 + SpecularLight3 + SpecularLight4;

	/*---|Get Texture Pixel|-------------------------*/
	float4 DiffuseMaterial = DiffuseMap.Sample( Trilinear, vOut.UV );
	float3 SpecularMaterial = DiffuseMaterial.a;

	/*---|The Rest|----------------------------------*/
	float4 combinedColour;
	combinedColour.rgb = DiffuseMaterial * DiffuseLight + SpecularMaterial * SpecularLight;
	combinedColour.a = 1.0f;

	return combinedColour;
}


float4 NormalMapPix( VS_NORMAL_OUTPUT vOut ) : SV_Target 
{
	/*---|Normal Calculations|----------------------*/
	float3 modelNormal = normalize(vOut.ModelNormal); 
	float3 modelTangent = normalize(vOut.ModelTangent);

	float3 modelBiTangent = cross( modelNormal, modelTangent );
	float3x3 invTangentMatrix = float3x3(modelTangent, modelBiTangent, modelNormal);

	float3 textureNormal = 2.0f * NormalMap.Sample( Trilinear, vOut.UV ) - 1.0f;

	float3 worldNormal = normalize( mul( mul( textureNormal, invTangentMatrix ), WorldMatrix ) );

	/*---|Lighting Calculations|--------------------*/
	float3 CameraDir = normalize(cameraPos - vOut.WorldPos.xyz);
	
	/*---|Light 1|----------------------------------*/
	float3 Light1Dir = normalize(light1Pos - vOut.WorldPos.xyz);
	float3 Light1Dist = length(light1Pos - vOut.WorldPos.xyz);
	float3 DiffuseLight1 = (light1Colour * max( dot(worldNormal.xyz, Light1Dir), 0 )) / Light1Dist*attenuation;
	float3 halfway = normalize(Light1Dir + CameraDir);
	float3 SpecularLight1 = DiffuseLight1 * pow( max( dot(worldNormal.xyz, halfway), 0 ), specularPower );

	/*---|Light 2|-----------------------------------*/
	float3 Light2Dir = normalize(light2Pos - vOut.WorldPos.xyz);
	float3 Light2Dist = length(light2Pos - vOut.WorldPos.xyz);
	float3 DiffuseLight2 = (light2Colour * max( dot(worldNormal.xyz, Light2Dir), 0 )) / Light2Dist*attenuation;
	halfway = normalize(Light2Dir + CameraDir);
	float3 SpecularLight2 = DiffuseLight2 * pow( max( dot(worldNormal.xyz, halfway), 0 ), specularPower );

	/*---|Light 3|-----------------------------------*/
	float3 Light3Dir = normalize(light3Pos - vOut.WorldPos.xyz);
	float3 Light3Dist = length(light3Pos - vOut.WorldPos.xyz);
	float3 DiffuseLight3 = (light3Colour * max( dot(worldNormal.xyz, Light3Dir), 0 )) / Light3Dist*attenuation;
	halfway = normalize(Light3Dir + CameraDir);
	float3 SpecularLight3 = DiffuseLight3 * pow( max( dot(worldNormal.xyz, halfway), 0 ), specularPower );

	/*---|Light 4|-----------------------------------*/
	float3 Light4Dir = normalize(light4Pos - vOut.WorldPos.xyz);
	float3 Light4Dist = length(light4Pos - vOut.WorldPos.xyz);
	float3 DiffuseLight4 = (light4Colour * max( dot(worldNormal.xyz, Light4Dir), 0 )) / Light4Dist*attenuation;
	halfway = normalize(Light4Dir + CameraDir);
	float3 SpecularLight4 = DiffuseLight4 * pow( max( dot(worldNormal.xyz, halfway), 0 ), specularPower );

	/*---|Sum of Lighting|---------------------------*/
	float3 DiffuseLight = ambientColour + DiffuseLight1 + DiffuseLight2 + DiffuseLight3 + DiffuseLight4;
	float3 SpecularLight = SpecularLight1 + SpecularLight2 + SpecularLight3 + SpecularLight4;

	/*---|Get Texture Pixel|-------------------------*/
	float4 DiffuseMaterial = DiffuseMap.Sample( Trilinear, vOut.UV );
	float3 SpecularMaterial = DiffuseMaterial.a;

	/*---|The Rest|----------------------------------*/
	float4 combinedColour;
	combinedColour.rgb = DiffuseMaterial * DiffuseLight + SpecularMaterial * SpecularLight;
	combinedColour.a = DiffuseMaterial.a;

	return combinedColour;
}

float4 ParallaxMapPix( VS_NORMAL_OUTPUT vOut ) : SV_Target 
{
	const float DepthAdjust = 0.001f;
	float3 halfway;

	float3 DiffuseLight1 = 0;
	float3 DiffuseLight2 = 0;
	float3 DiffuseLight3 = 0;
	float3 DiffuseLight4 = 0;

	float3 SpecularLight1 = 0;
	float3 SpecularLight2 = 0;
	float3 SpecularLight3 = 0;
	float3 SpecularLight4 = 0;

	/*---|Normal Calculations|----------------------*/
	float3 modelNormal = normalize(vOut.ModelNormal); 
	float3 modelTangent = normalize(vOut.ModelTangent);

	float3 modelBiTangent = cross( modelNormal, modelTangent );
	float3x3 invTangentMatrix = float3x3(modelTangent, modelBiTangent, modelNormal);

	/*---|Parallax Calculation|---------------------*/
	float3 CameraDir = normalize(cameraPos - vOut.WorldPos.xyz);

	float3x3 invWorldMatrix = transpose( WorldMatrix );
	float3 cameraModelDir = normalize( mul( CameraDir, invWorldMatrix ) );

	float3x3 tangentMatrix = transpose( invWorldMatrix ); 
	float2 textureOffsetDir = mul( cameraModelDir, tangentMatrix );

	float texDepth = ParallaxDepth * (NormalMap.Sample( Trilinear, vOut.UV ).a - 0.5f);

	float2 offsetTexCoord = vOut.UV + texDepth * textureOffsetDir;

	float3 textureNormal = 2.0f * NormalMap.Sample( Trilinear, offsetTexCoord ) - 1.0f; 
	float3 worldNormal = normalize( mul( mul( textureNormal, invTangentMatrix ), WorldMatrix ) );
	
	/*---|Lighting Calculations|--------------------*/
	/*---|Light 1 - Spot Light|---------------------*/
	float4 light1ViewPos = mul( float4(vOut.WorldPos, 1.0f), light1ViewMatrix ); 
	float4 light1ProjPos = mul( light1ViewPos, light1ProjMatrix );

	float3 Light1Dir = normalize(light1Pos - vOut.WorldPos.xyz);

	if (dot(light1Facing, -Light1Dir)>light1CosHalfAngle )
	{
		float2 shadowUV = 0.5f * light1ProjPos.xy / light1ProjPos.w + float2( 0.5f, 0.5f );
		shadowUV.y = 1.0f - shadowUV.y;
	
		float depthFromLight = light1ProjPos.z / light1ProjPos.w - DepthAdjust;
	
		if (depthFromLight < ShadowMap1.Sample( PointSampleClamp, shadowUV ).r)
		{
			float3 Light1Dist = length(light1Pos - vOut.WorldPos.xyz);
			DiffuseLight1 = (light1Colour * max( dot(worldNormal.xyz, Light1Dir), 0 )) / Light1Dist*attenuation;
			halfway = normalize(Light1Dir + CameraDir);
			SpecularLight1 = DiffuseLight1 * pow( max( dot(worldNormal.xyz, halfway), 0 ), specularPower );
		}
	}

	/*---|Light 2 - Spot Light|---------------------*/
	float4 light2ViewPos = mul( float4(vOut.WorldPos, 1.0f), light2ViewMatrix ); 
	float4 light2ProjPos = mul( light2ViewPos, light2ProjMatrix );

	float3 Light2Dir = normalize(light2Pos - vOut.WorldPos.xyz);
	if (dot(light2Facing, -Light2Dir)>light2CosHalfAngle )
	{
		float2 shadowUV = 0.5f * light2ProjPos.xy / light2ProjPos.w + float2( 0.5f, 0.5f );
		shadowUV.y = 1.0f - shadowUV.y;
	
		float depthFromLight = light2ProjPos.z / light2ProjPos.w - DepthAdjust;
	
		if (depthFromLight < ShadowMap2.Sample( PointSampleClamp, shadowUV ).r)
		{
			float3 Light2Dist = length(light2Pos - vOut.WorldPos.xyz);
			DiffuseLight2 = (light2Colour * max( dot(worldNormal.xyz, Light2Dir), 0 )) / Light2Dist*attenuation;
			halfway = normalize(Light2Dir + CameraDir);
			SpecularLight2 = DiffuseLight2 * pow( max( dot(worldNormal.xyz, halfway), 0 ), specularPower );
		}
	}

	/*---|Light 3 - Point Light|---------------------*/
	float3 Light3Dir = normalize(light3Pos - vOut.WorldPos.xyz);
	float3 Light3Dist = length(light3Pos - vOut.WorldPos.xyz);
	DiffuseLight3 = (light3Colour * max( dot(worldNormal.xyz, Light3Dir), 0 )) / Light3Dist*attenuation;
	halfway = normalize(Light3Dir + CameraDir);
	SpecularLight3 = DiffuseLight3 * pow( max( dot(worldNormal.xyz, halfway), 0 ), specularPower );

	/*---|Light 4 - Torch Light|---------------------*/
	float4 light4ViewPos = mul( float4(vOut.WorldPos, 1.0f), light4ViewMatrix ); 
	float4 light4ProjPos = mul( light4ViewPos, light4ProjMatrix );

	float3 Light4Dir = normalize(light4Pos - vOut.WorldPos.xyz);

	if (dot(light4Facing, Light4Dir)>light4CosHalfAngle )
	{
		float2 shadowUV = 0.5f * light4ProjPos.xy / light4ProjPos.w + float2( 0.5f, 0.5f );
		shadowUV.y = 1.0f - shadowUV.y;
	
		float depthFromLight = light4ProjPos.z / light4ProjPos.w - DepthAdjust;
	
		if (depthFromLight < ShadowMap3.Sample( PointSampleClamp, shadowUV ).r)
		{
			float3 Light4Dist = length(light4Pos - vOut.WorldPos.xyz);
			DiffuseLight4 = (light4Colour * max( dot(worldNormal.xyz, Light4Dir), 0 )) / Light4Dist*attenuation;
			halfway = normalize(Light4Dir + CameraDir);
			SpecularLight4 = DiffuseLight4 * pow( max( dot(worldNormal.xyz, halfway), 0 ), specularPower );
		}
	}

	/*---|Sum of Lighting|---------------------------*/
	float3 DiffuseLight = ambientColour + DiffuseLight1 + DiffuseLight2 + DiffuseLight3 + DiffuseLight4;
	float3 SpecularLight = SpecularLight1 + SpecularLight2 + SpecularLight3 + SpecularLight4;

	/*---|Get Texture Pixel|-------------------------*/
	float4 DiffuseMaterial = DiffuseMap.Sample( Trilinear, offsetTexCoord );
	float3 SpecularMaterial = DiffuseMaterial.a;

	/*---|The Rest|----------------------------------*/
	float4 combinedColour;
	combinedColour.rgb = DiffuseMaterial * DiffuseLight + SpecularMaterial * SpecularLight;
	combinedColour.a = 1.0f;

	return combinedColour;
}



float4 CellShadedPix(VS_LIGHTING_OUTPUT vOut) : SV_TARGET
{
	float3 worldNormal = normalize(vOut.WorldNormal); 

	float3 CameraDir = normalize(cameraPos - vOut.WorldPos.xyz);

	/*---|Lighting Calculations|--------------------*/
	/*---|Light 1|----------------------------------*/
	float3 Light1Dir = normalize(light1Pos - vOut.WorldPos.xyz);
	float3 Light1Dist = length(light1Pos - vOut.WorldPos.xyz); 

	float DiffuseLevel1 = max( dot(worldNormal.xyz, Light1Dir), 0 );
	float CellDiffuseLevel1 = CellMap.Sample( PointSampleClamp, DiffuseLevel1 ).r;
	float3 DiffuseLight1 = light1Colour * CellDiffuseLevel1 / Light1Dist*attenuation;

	float3 halfway = normalize(Light1Dir + CameraDir);
	float SpecularLevel1 = pow( max( dot(worldNormal.xyz, halfway), 0 ), specularPower );
	float CellSpecularLevel1 = CellMap.Sample( PointSampleClamp, SpecularLevel1 ).r;
	float3 SpecularLight1 = DiffuseLight1 * CellSpecularLevel1;

	/*---|Light 2|----------------------------------*/
	float3 Light2Dir = normalize(light2Pos - vOut.WorldPos.xyz);
	float3 Light2Dist = length(light2Pos - vOut.WorldPos.xyz);
	float DiffuseLevel2 = max( dot(worldNormal.xyz, Light2Dir), 0 );
	float CellDiffuseLevel2 = CellMap.Sample( PointSampleClamp, DiffuseLevel2 ).r;
	float3 DiffuseLight2 = light2Colour * CellDiffuseLevel2 / Light2Dist*attenuation;

	halfway = normalize(Light2Dir + CameraDir);
	float SpecularLevel2 = pow( max( dot(worldNormal.xyz, halfway), 0 ), specularPower );
	float CellSpecularLevel2 = CellMap.Sample( PointSampleClamp, SpecularLevel2 ).r;
	float3 SpecularLight2 = DiffuseLight2 * CellSpecularLevel2;

	//No Light 3 because its too far away for me to care

	/*---|Sum of Lighting|---------------------------*/
	float3 DiffuseLight = ambientColour + DiffuseLight1 + DiffuseLight2;
	float3 SpecularLight = SpecularLight1 + SpecularLight2;

	float4 DiffuseMaterial = DiffuseMap.Sample( Trilinear, vOut.UV );
	
	float3 SpecularMaterial = DiffuseMaterial.a;

	float4 combinedColour;
	combinedColour.rgb = DiffuseMaterial * DiffuseLight + SpecularMaterial * SpecularLight;
	combinedColour.a = 1.0f;

	return combinedColour;
}

float4 TintDiffusePix( VS_TEXTURE_OUTPUT vOut ) : SV_Target
{
	float4 diffuseMapColour = DiffuseMap.Sample( Trilinear, vOut.UV );

	diffuseMapColour.rgb *= ModelColour / 10;

	return diffuseMapColour;
}

float4 PixelDepthPix( VS_BASIC_OUTPUT vOut ) : SV_Target
{
	return vOut.ProjPos.z / vOut.ProjPos.w;
}

float4 CubeMapPix( VS_CUBEMAP_OUTPUT vOut ) : SV_Target
{
	return CubeMapTexture.Sample(Trilinear, vOut.UV);
}

float4 CutoutPix( VS_TEXTURE_OUTPUT vOut ) : SV_Target
{
	float4 diffuseMapColour = DiffuseMap.Sample( Trilinear, vOut.UV );
	if(diffuseMapColour.a < 0.5)
		discard;

	return diffuseMapColour;
}

float4 MirrorLitDiffuseMapPix( VS_LIGHTING_OUTPUT vOut ) : SV_Target
{
	float3 worldNormal = normalize(vOut.WorldNormal); 


	///////////////////////
	// Calculate lighting

	float3 CameraDir = normalize(cameraPos - vOut.WorldPos.xyz);

	//// LIGHT 1
	float3 Light1Dir = normalize(light1Pos - vOut.WorldPos.xyz);
	float3 Light1Dist = length(light1Pos - vOut.WorldPos.xyz); 
	float3 DiffuseLight1 = light1Colour * max( dot(worldNormal.xyz, Light1Dir), 0 ) / Light1Dist*attenuation;
	float3 halfway = normalize(Light1Dir + CameraDir);
	float3 SpecularLight1 = DiffuseLight1 * pow( max( dot(worldNormal.xyz, halfway), 0 ), specularPower );

	//// LIGHT 2
	float3 Light2Dir = normalize(light2Pos - vOut.WorldPos.xyz);
	float3 Light2Dist = length(light2Pos - vOut.WorldPos.xyz);
	float3 DiffuseLight2 = light2Colour * max( dot(worldNormal.xyz, Light2Dir), 0 ) / Light2Dist*attenuation;
	halfway = normalize(Light2Dir + CameraDir);
	float3 SpecularLight2 = DiffuseLight2 * pow( max( dot(worldNormal.xyz, halfway), 0 ), specularPower );

	float3 DiffuseLight = ambientColour + DiffuseLight1 + DiffuseLight2;
	float3 SpecularLight = SpecularLight1 + SpecularLight2;


	////////////////////
	// Sample texture

	float4 DiffuseMaterial = DiffuseMap.Sample( Trilinear, vOut.UV );
	
	float3 SpecularMaterial = DiffuseMaterial.a;

	
	////////////////////
	// Combine colours 
	
	float4 combinedColour;
	combinedColour.rgb = DiffuseMaterial.rgb * DiffuseLight + SpecularMaterial * SpecularLight;
	combinedColour.a = 1.0f;
	return combinedColour;
}
PS_DEPTH_OUTPUT ClearDepth( VS_BASIC_OUTPUT vOut )
{
	PS_DEPTH_OUTPUT pOut;

	pOut.Colour = float4(ModelColour, 1.0f);
	pOut.Depth = 1.0f;

	return pOut;
}

//--------------------------------------------------------------------------------------
// States
//--------------------------------------------------------------------------------------

RasterizerState CullNone
{
	CullMode = None;
};
RasterizerState CullBack
{
	CullMode = Back;
};
RasterizerState CullFront
{
	CullMode = Front;
};


DepthStencilState DepthWritesOn  
{
	DepthFunc      = Less;  // Must remember reset states that are changed in other techniques - we use the depth buffer normally here
	DepthWriteMask = All; 
	StencilEnable  = False; // Also switch off states we don't need from other techniques
};

// Don't write to the depth buffer - polygons rendered will not obscure other polygons rendered later - used for blending modes (we covered this earlier)
DepthStencilState DepthWritesOff 
{
	// Test the depth buffer, but don't write anything new to it
	DepthFunc      = Less;
	DepthWriteMask = Zero;
	StencilEnable  = False;
};

DepthStencilState SetStencilValue
{
	// Use depth buffer normally
	DepthFunc      = Less;
	DepthWriteMask = All;

	// Enable stencil buffer and replace all pixel stencil values with the reference value (value specified in the technique)
	StencilEnable        = True;
	FrontFaceStencilFunc = Always;  // Always...
	FrontFaceStencilPass = Replace; // ...replace the stencil values
	BackFaceStencilFunc  = Always;
	BackFaceStencilPass  = Replace; 
};

DepthStencilState AffectStencilArea
{
	// Use depth buffer normally
	DepthFunc      = Less;
	DepthWriteMask = All;

	// Only render those pixels whose stencil value is equal to the reference value (value specified in the technique)
	StencilEnable        = True;
	FrontFaceStencilFunc = Equal; // Only render on matching stencil
	FrontFaceStencilPass = Keep;  // But don't change the stencil values
	BackFaceStencilFunc  = Equal;
	BackFaceStencilPass  = Keep; 
};

DepthStencilState AffectStencilAreaDepthWritesOff
{
	// Test the depth buffer, but don't write anything new to it
	DepthFunc      = Less;
	DepthWriteMask = Zero; 

	// Only render those pixels whose stencil value is equal to the reference value (value specified in the technique)
	StencilEnable        = True;
	FrontFaceStencilFunc = Equal; // Only render on matching stencil
	FrontFaceStencilPass = Keep;  // But don't change the stencil values
	BackFaceStencilFunc  = Equal;
	BackFaceStencilPass  = Keep; 
};

DepthStencilState AffectStencilAreaIgnoreDepth
{
	// Disable depth buffer - we're going to fill the mirror/portal with distant z-values as we will want to render a new scene in there. So we must
	// ignore the z-values of the mirror surface - or the depth buffer would think the mirror polygon was obscuring our distant z-values
	DepthFunc = Always;

	// Only render those pixels whose stencil value is equal to the reference value (value specified in the technique)
	StencilEnable        = True;
	FrontFaceStencilFunc = Equal; // Only render on matching stencil
	FrontFaceStencilPass = Keep;  // But don't change the stencil values
	BackFaceStencilFunc  = Equal;
	BackFaceStencilPass  = Keep; 
};


BlendState NoBlending
{
    BlendEnable[0] = FALSE;
};

BlendState AdditiveBlending
{
    BlendEnable[0] = TRUE;
    SrcBlend = ONE;
    DestBlend = ONE;
    BlendOp = ADD;
};

BlendState Alpha
{
	BlendEnable[0] = TRUE;
	SrcBlend = SRC_ALPHA;
	DestBlend = INV_SRC_ALPHA;
	BlendOp = ADD;
};

BlendState NoColourOutput // Use blending to prevent drawing pixel colours, but still allow depth/stencil updates
{
    BlendEnable[0] = TRUE;
    SrcBlend = ZERO;
    DestBlend = ONE;
    BlendOp = ADD;
};

//--------------------------------------------------------------------------------------
// Techniques
//--------------------------------------------------------------------------------------

// Techniques are used to render models in our scene. They select a combination of vertex, geometry and pixel shader from those provided above. Can also set states.

// Render models unlit in a single colour
technique10 PlainColour
{
    pass P0
    {
        SetVertexShader( CompileShader( vs_4_0, BasicTransformVer() ) );
        SetGeometryShader( NULL );                                   
        SetPixelShader( CompileShader( ps_4_0, OneColourPix() ) );

		SetRasterizerState( CullBack );
	}
}

technique10 Textured
{
    pass P0
    {
        SetVertexShader( CompileShader( vs_4_0, BasicTransformVer() ) );
        SetGeometryShader( NULL );                                   
        SetPixelShader( CompileShader( ps_4_0, TexturedPix() ) );

		SetRasterizerState( CullBack );
	}
}

technique10 MovingTexture
{
    pass P0
    {
        SetVertexShader( CompileShader( vs_4_0, WiggleTextureVer() ) );
        SetGeometryShader( NULL );                                   
        SetPixelShader( CompileShader( ps_4_0, ScrollTexturePix() ) );

		SetRasterizerState( CullBack );
	}
}

technique10 DiffuseSpecular
{
    pass P0
    {
        SetVertexShader( CompileShader( vs_4_0, DiffuseSpecularVer() ) );
        SetGeometryShader( NULL );                                   
        SetPixelShader( CompileShader( ps_4_0, DiffuseSpecularPix() ) );

		SetRasterizerState( CullNone );
	}
}

technique10 NormalMapping
{
    pass P0
    {
        SetVertexShader( CompileShader( vs_4_0, NormalMapVer() ) );
        SetGeometryShader( NULL );                                   
        SetPixelShader( CompileShader( ps_4_0, NormalMapPix() ) );

		SetRasterizerState( CullBack );
	}
}

technique10 ParallaxMapping
{
    pass P0
    {
        SetVertexShader( CompileShader( vs_4_0, NormalMapVer() ) );
        SetGeometryShader( NULL );                                   
        SetPixelShader( CompileShader( ps_4_0, ParallaxMapPix() ) );

		SetBlendState( NoBlending, float4( 0.0f, 0.0f, 0.0f, 0.0f ), 0xFFFFFFFF );
		SetRasterizerState( CullNone ); 
		SetDepthStencilState( DepthWritesOn, 0 );
	}
}

technique10 CellShading
{
    pass P0
    {
        SetVertexShader( CompileShader( vs_4_0, OutlineVer() ) );
        SetGeometryShader( NULL );                                   
        SetPixelShader( CompileShader( ps_4_0, OneColourPix() ) );

		SetRasterizerState( CullFront ); 
		SetBlendState( NoBlending, float4( 0.0f, 0.0f, 0.0f, 0.0f ), 0xFFFFFFFF );
		SetDepthStencilState( DepthWritesOn, 0 );
	}
    pass P1
    {
        SetVertexShader( CompileShader( vs_4_0, DiffuseSpecularVer() ) );
        SetGeometryShader( NULL );                                   
        SetPixelShader( CompileShader( ps_4_0, CellShadedPix() ) );

		SetRasterizerState( CullBack );
	}
}

technique10 CubeMap
{
	pass P0
	{
		SetVertexShader( CompileShader( vs_4_0, CubeMapVer() ) );
        SetGeometryShader( NULL );                                   
        SetPixelShader( CompileShader( ps_4_0, CubeMapPix() ) );

		SetBlendState( NoBlending, float4( 0.0f, 0.0f, 0.0f, 0.0f ), 0xFFFFFFFF );
		SetRasterizerState( CullNone ); 
		SetDepthStencilState( DepthWritesOn, 0 );
	}
}

technique10 DepthOnly
{
    pass P0
    {
        SetVertexShader( CompileShader( vs_4_0, BasicTransformVer() ) );
        SetGeometryShader( NULL );                                   
        SetPixelShader( CompileShader( ps_4_0, PixelDepthPix() ) );

		// Switch off blending states
		SetBlendState( NoBlending, float4( 0.0f, 0.0f, 0.0f, 0.0f ), 0xFFFFFFFF );
		SetRasterizerState( CullBack ); 
		SetDepthStencilState( DepthWritesOn, 0 );
     }
}

technique10 Cutout
{
    pass P0
    {
        SetVertexShader( CompileShader( vs_4_0, BasicTransformVer() ) );
        SetGeometryShader( NULL );                                   
        SetPixelShader( CompileShader( ps_4_0, CutoutPix() ) );

		// Switch off blending states
		SetBlendState( NoBlending, float4( 0.0f, 0.0f, 0.0f, 0.0f ), 0xFFFFFFFF );
		SetRasterizerState( CullNone ); 
		SetDepthStencilState( DepthWritesOn, 0 );
     }
}

/*---|Mirrors|--------------------------------------------------------*/
technique10 Mirror
{
    pass P0 // Set the stencil values to 1, do nothing to the pixels
    {
        SetVertexShader( CompileShader( vs_4_0, MirrorVer() ) );
        SetGeometryShader( NULL );                                   
        SetPixelShader( CompileShader( ps_4_0, OneColourPix() ) );

		// Switch off colour output (only updating stencil in this pass), normal culling
		SetBlendState( NoColourOutput, float4( 0.0f, 0.0f, 0.0f, 0.0f ), 0xFFFFFFFF );
		SetRasterizerState( CullBack ); 

		// Set the stencil to 1 in the visible area of this polygon
		SetDepthStencilState( SetStencilValue, 1 );
	}
    pass P1 // Set the pixel colour to background colour, set z-value to furthest distance - but do this only where stencil was set to 1 above
    {
        SetVertexShader( CompileShader( vs_4_0, MirrorVer() ) );
        SetGeometryShader( NULL );                                   
        SetPixelShader( CompileShader( ps_4_0, ClearDepth() ) );

		// Switch off blending, normal culling
		SetBlendState( NoBlending, float4( 0.0f, 0.0f, 0.0f, 0.0f ), 0xFFFFFFFF );
		SetRasterizerState( CullBack ); 

		// Only affect the area where the stencil was set to 1 in the pass above
		SetDepthStencilState( AffectStencilAreaIgnoreDepth, 1 );
	}
}

technique10 MirrorSurface
{
    pass P0
    {
        SetVertexShader( CompileShader( vs_4_0, MirrorVer() ) );
        SetGeometryShader( NULL );                                   
        SetPixelShader( CompileShader( ps_4_0, OneColourPix() ) );

		// Use a special blending state to disable any changes to the viewport pixels as we're only updating the stencil/depth
		// buffer in this pass (for now).
		SetBlendState( NoColourOutput, float4( 0.0f, 0.0f, 0.0f, 0.0f ), 0xFFFFFFFF ); 

		// Standard culling
		SetRasterizerState( CullBack ); 

		// Set the stencil back to 0 in the surface of the polygon
		SetDepthStencilState( SetStencilValue, 0 );
	}
}

technique10 Additive
{
    pass P0
    {
        SetVertexShader( CompileShader( vs_4_0, BasicTransformVer() ) );
        SetGeometryShader( NULL );                                   
        SetPixelShader( CompileShader( ps_4_0, TexturedPix() ) );

		SetBlendState( AdditiveBlending, float4( 0.0f, 0.0f, 0.0f, 0.0f ), 0xFFFFFFFF );
		SetRasterizerState( CullNone ); 
		SetDepthStencilState( DepthWritesOff, 0 );
     }
}