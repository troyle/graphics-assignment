#include "Light.h"
#include "Defines.h"

D3DXMATRIXA16 LightViewMatrix( CLight* Light )
{
	D3DXMATRIXA16 viewMatrix;
	
	D3DXMATRIXA16 worldMatrix = Light->GetWorldMatrix();
	D3DXMatrixInverse( &viewMatrix, NULL, &worldMatrix );

	return viewMatrix;
}

D3DXMATRIXA16 LightProjMatrix()
{
	D3DXMATRIXA16 projMatrix;

	// Create a projection matrix for the light. Use the spotlight cone angle as an FOV, just set default values for everything else.
	//D3DXMatrixPerspectiveFovLH( &projMatrix, ToRadians( SpotlightConeAngle ), 1, 0.005f, 100000.0f);
	D3DXMatrixPerspectiveFovLH( &projMatrix, ToRadians( SpotlightConeAngle ), 1, 1.0f, 1000.0f);
	return projMatrix;
}

CPointLight::CPointLight(ID3D10EffectTechnique* Technique)
{
	m_Light = new CModel;
	m_Light->Load( "Light.x", Technique );
	m_LightPos = NULL;
	m_LightColour = NULL;
}
void CPointLight::LinkToShader(ID3D10Effect* &Effect,LPCSTR Pos,LPCSTR Col,LPCSTR Face,LPCSTR View,LPCSTR Proj,LPCSTR Cos)
{
	m_LightPos        = Effect->GetVariableByName( Pos  )->AsVector();
	m_LightColour     = Effect->GetVariableByName( Col  )->AsVector();
}
void CPointLight::SendToShader()
{
	m_LightPos->	SetRawValue( m_Light->GetPosition(), 0, 12 );
	m_LightColour->	SetRawValue( m_Colour, 0, 12 );
}
void CPointLight::MorphColour(double amount)
{
	m_Colour.x = abs(sin(amount));
	m_Colour.y = abs(sin(amount+10));
	m_Colour.z = abs(sin(amount+5));
}
void CPointLight::MorphBrightness(double amount)
{
	m_Colour.x = abs(sin(amount));
	m_Colour.y = abs(sin(amount));
	m_Colour.z = abs(sin(amount));
}
CPointLight::~CPointLight()
{
	delete m_Light;
}

CSpotLight::CSpotLight(ID3D10EffectTechnique* Technique)
{
	m_Light = new CModel;
	m_Light->Load( "Sphere.x", Technique );
	m_LightPos = NULL;
	m_LightFacing = NULL;
	m_LightViewMatrix = NULL;
	m_LightProjMatrix = NULL;
	m_LightConeAngle = NULL;
	m_LightColour = NULL;
}
void CSpotLight::LinkToShader(ID3D10Effect* &Effect,LPCSTR Pos,LPCSTR Col,LPCSTR Face,LPCSTR View,LPCSTR Proj,LPCSTR Cos)
{
	m_LightPos        = Effect->GetVariableByName( Pos  )->AsVector();
	m_LightColour     = Effect->GetVariableByName( Col  )->AsVector();
	m_LightFacing     = Effect->GetVariableByName( Face )->AsVector();
	m_LightViewMatrix = Effect->GetVariableByName( View )->AsMatrix();
	m_LightProjMatrix = Effect->GetVariableByName( Proj )->AsMatrix();
	m_LightConeAngle  = Effect->GetVariableByName( Cos	)->AsScalar();
}
void CSpotLight::SendToShader()
{
	m_LightPos->		SetRawValue( m_Light->GetPosition(), 0, 12 );
	m_LightFacing->		SetRawValue( m_Light->Facing(), 0, 12 );
	m_LightViewMatrix->	SetMatrix( LightViewMatrix( this ) );
	m_LightProjMatrix->	SetMatrix( (float*)LightProjMatrix(  ) );
	m_LightConeAngle->	SetFloat( cos( ToRadians(SpotlightConeAngle * 0.5f) ) );
	m_LightColour->		SetRawValue( m_Colour, 0, 12 );
}
void CSpotLight::MorphColour(double amount)
{
	m_Colour.x = abs(sin(amount));
	m_Colour.y = abs(sin(amount+10));
	m_Colour.z = abs(sin(amount+5));
}
void CSpotLight::MorphBrightness(double amount)
{
	m_Colour.x = abs(sin(amount));
	m_Colour.y = abs(sin(amount));
	m_Colour.z = abs(sin(amount));
}
CSpotLight::~CSpotLight()
{
	delete m_Light;
}
