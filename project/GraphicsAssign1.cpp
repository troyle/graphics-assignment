//--------------------------------------------------------------------------------------
//	GraphicsAssign1.cpp
//
//	Shaders Graphics Assignment
//	Add further models using different shader techniques
//	See assignment specification for details
//--------------------------------------------------------------------------------------

//***|  INFO  |****************************************************************
// Lights:
//   The initial project shows models for a couple of point lights, but the
//   initial shaders don't actually apply any lighting. Part of the assignment
//   is to add a shader that uses this information to actually light a model.
//   Refer to lab work to determine how best to do this.
// 
// Textures:
//   The models in the initial scene have textures loaded but the initial
//   technique/shaders doesn't show them. Part of the assignment is to add 
//   techniques to show a textured model
//
// Shaders:
//   The initial shaders render plain coloured objects with no lighting:
//   - The vertex shader performs basic transformation of 3D vertices to 2D
//   - The pixel shader colours every pixel the same colour
//   A few shader variables are set up to communicate between C++ and HLSL
//   You will usually need to add further variables to add new techniques
//*****************************************************************************

#include <windows.h>
#include <d3d10.h>
#include <d3dx10.h>
#include <atlbase.h>
#include "resource.h"

#include "Defines.h" // General definitions shared by all source files
#include "Model.h"   // Model class - encapsulates working with vertex/index data and world matrix
#include "Camera.h"  // Camera class - encapsulates the camera's view and projection matrix
#include "Input.h"   // Input functions - not DirectX
#include "Light.h"
//#include "MatrixCalculations.h"


//--------------------------------------------------------------------------------------
// Global Scene Variables
//--------------------------------------------------------------------------------------

// Models and cameras encapsulated in classes for flexibity and convenience
// The CModel class collects together geometry and world matrix, and provides functions to control the model and render it
// The CCamera class handles the view and projections matrice, and provides functions to control the camera
CModel* Cube;
CModel* Floor;
CModel* Sphere;
CCamera* Camera;
CModel* Teapot;
CModel* Hills;
CModel* Troll;
CModel* SkyBox;
CModel* Ball;
CModel* Grenade;
CModel* Torch;
CModel* CutOut;
CModel* Mirror;

// Textures - no texture class yet so using DirectX variables
ID3D10ShaderResourceView* CubeDiffuseMap	= NULL;
ID3D10ShaderResourceView* CubeNormalMap		= NULL;
ID3D10ShaderResourceView* FloorDiffuseMap	= NULL;
ID3D10ShaderResourceView* SphereDiffuseMap	= NULL;
ID3D10ShaderResourceView* FloorNormalMap	= NULL;
ID3D10ShaderResourceView* TeapotDiffuseMap	= NULL;
ID3D10ShaderResourceView* TeapotNormalMap	= NULL;
ID3D10ShaderResourceView* HillsDiffuseMap	= NULL;
ID3D10ShaderResourceView* HillsNormalMap	= NULL;
ID3D10ShaderResourceView* GrenadeDiffuseMap	= NULL;
ID3D10ShaderResourceView* GrenadeNormalMap	= NULL;
ID3D10ShaderResourceView* TrollDiffuseMap	= NULL;
ID3D10ShaderResourceView* TorchDiffuseMap	= NULL;
ID3D10ShaderResourceView* CubeMapDiffuseMap	= NULL;
ID3D10ShaderResourceView* CutOutDiffuseMap	= NULL;
ID3D10ShaderResourceView* LightDiffuseMap	= NULL;
ID3D10ShaderResourceView* CellMap = NULL;

D3DXVECTOR3 OutlineColour    = D3DXVECTOR3( 0, 0, 0 );
float       OutlineThickness = 0.012f;

ID3D10Texture2D*          ShadowMap1Texture		= NULL;
ID3D10DepthStencilView*   ShadowMap1DepthView	= NULL;
ID3D10ShaderResourceView* ShadowMap1			= NULL;
ID3D10Texture2D*          ShadowMap2Texture		= NULL;
ID3D10DepthStencilView*   ShadowMap2DepthView	= NULL;
ID3D10ShaderResourceView* ShadowMap2			= NULL;
ID3D10Texture2D*          ShadowMap3Texture		= NULL;
ID3D10DepthStencilView*   ShadowMap3DepthView	= NULL;
ID3D10ShaderResourceView* ShadowMap3			= NULL;

int ShadowMapSize = 256*8;

// Light data - stored manually as there is no light class
const int MAX_LIGHTS = 3;
CLight* Light[MAX_LIGHTS];

D3DXVECTOR3 Light1Colour = D3DXVECTOR3( 1.0f, 0.0f, 0.7f );
D3DXVECTOR3 Light2Colour = D3DXVECTOR3( 1.0f, 0.8f, 0.2f );
D3DXVECTOR3 Light3Colour = D3DXVECTOR3( 1.0f, 0.8f, 0.5f );
D3DXVECTOR3 Light4Colour = D3DXVECTOR3( 1.0f, 1.0f, 1.0f );

float SpecularPower = 256.0f;
float Attenuation = 24.0f;
D3DXVECTOR3 AmbientColour = D3DXVECTOR3( 0.2f, 0.2f, 0.2f );

Technique technique;
bool tangents;

float ParallaxDepth = 0.05f;

// Display models where the lights are. One of the lights will follow an orbit
const float LightOrbitRadius = 20.0f;
const float LightOrbitSpeed  = 0.5f;

// Note: There are move & rotation speed constants in Defines.h

//--------------------------------------------------------------------------------------
// Shader Variables
//--------------------------------------------------------------------------------------
// Variables to connect C++ code to HLSL shaders

// Effects / techniques
ID3D10Effect*          Effect					= NULL;
ID3D10EffectTechnique* PlainColourTechnique		= NULL;
ID3D10EffectTechnique* TexturedTechnique		= NULL;
ID3D10EffectTechnique* MovingTechnique			= NULL;
ID3D10EffectTechnique* DiffuseSpecularTechnique = NULL;
ID3D10EffectTechnique* NormalMappingTechnique	= NULL;
ID3D10EffectTechnique* ParallaxMappingTechnique	= NULL;
ID3D10EffectTechnique* CellShadingTechnique		= NULL;
ID3D10EffectTechnique* CubeMapTechnique			= NULL;
ID3D10EffectTechnique* ReflectionTechnique		= NULL;
ID3D10EffectTechnique* DepthOnlyTechnique		= NULL;
ID3D10EffectTechnique* CutoutTechnique			= NULL;
ID3D10EffectTechnique* MirrorTechnique			= NULL;
ID3D10EffectTechnique* MirrorSurfaceTechnique	= NULL;
ID3D10EffectTechnique* AdditiveTechnique		= NULL;

ID3D10EffectTechnique* currentTechnique = NULL;

// Matrices
ID3D10EffectMatrixVariable* WorldMatrixVar		= NULL;
ID3D10EffectMatrixVariable* ViewMatrixVar		= NULL;
ID3D10EffectMatrixVariable* ProjMatrixVar		= NULL;
ID3D10EffectMatrixVariable* ViewProjMatrixVar	= NULL;
ID3D10EffectMatrixVariable* InverseMatrixVar	= NULL;

D3DMATRIX InverseMatrix;

// Textures
ID3D10EffectShaderResourceVariable* DiffuseMapVar = NULL;
ID3D10EffectShaderResourceVariable* NormalMapVar  = NULL;
ID3D10EffectShaderResourceVariable* CellMapVar	  = NULL;

ID3D10EffectShaderResourceVariable* ShadowMap1Var = NULL;
ID3D10EffectShaderResourceVariable* ShadowMap2Var = NULL;
ID3D10EffectShaderResourceVariable* ShadowMap3Var = NULL;

ID3D10EffectShaderResourceVariable* CubeMapVar	  = NULL;

// Miscellaneous
ID3D10EffectVectorVariable* ModelColourVar		= NULL;

//Added by me

ID3D10EffectVectorVariable* AmbientColourVar	= NULL;
ID3D10EffectScalarVariable* SpecularPowerVar	= NULL;
ID3D10EffectScalarVariable* AttenuationVar		= NULL;
//Misc
ID3D10EffectVectorVariable* Tint				= NULL;
ID3D10EffectScalarVariable* Amount				= NULL;
ID3D10EffectVectorVariable* CameraPosVar		= NULL;
ID3D10EffectScalarVariable* ParallaxDepthVar	= NULL;
ID3D10EffectScalarVariable* OutlineThicknessVar = NULL;

ID3D10EffectVectorVariable* ClipPlaneVar = NULL;
//Shadows


float wiggle;


//--------------------------------------------------------------------------------------
// DirectX Variables
//--------------------------------------------------------------------------------------

// The main D3D interface, this pointer is used to access most D3D functions (and is shared across all cpp files through Defines.h)
ID3D10Device* g_pd3dDevice = NULL;

// Width and height of the window viewport
int g_ViewportWidth;
int g_ViewportHeight;

// Variables used to setup D3D
IDXGISwapChain*         SwapChain = NULL;
ID3D10Texture2D*        DepthStencil = NULL;
ID3D10DepthStencilView* DepthStencilView = NULL;
ID3D10RenderTargetView* RenderTargetView = NULL;

//---|Portal Data|----------------------------------------------------------------------
int PortalWidth  = 2048;
int PortalHeight = 2048;

CModel*  Portal;       // The model on which the portal appears
CCamera* PortalCamera; // The camera view shown in the portal

// The portal texture and the view of it as a render target (see code comments)
ID3D10Texture2D*          PortalTexture = NULL;
ID3D10RenderTargetView*   PortalRenderTarget = NULL;
ID3D10ShaderResourceView* PortalMap = NULL;

// Also need a depth/stencil buffer for the portal
// NOTE: ***Can share the depth buffer between multiple portals of the same size***
ID3D10Texture2D*        PortalDepthStencil = NULL;
ID3D10DepthStencilView* PortalDepthStencilView = NULL;


//--------------------------------------------------------------------------------------
// Create Direct3D device and swap chain
//--------------------------------------------------------------------------------------
bool InitDevice(HWND hWnd)
{
	// Many DirectX functions return a "HRESULT" variable to indicate success or failure. Microsoft code often uses
	// the FAILED macro to test this variable, you'll see it throughout the code - it's fairly self explanatory.
	HRESULT hr = S_OK;


	////////////////////////////////
	// Initialise Direct3D

	// Calculate the visible area the window we are using - the "client rectangle" refered to in the first function is the 
	// size of the interior of the window, i.e. excluding the frame and title
	RECT rc;
	GetClientRect(hWnd, &rc);
	g_ViewportWidth = rc.right - rc.left;
	g_ViewportHeight = rc.bottom - rc.top;


	// Create a Direct3D device (i.e. initialise D3D), and create a swap-chain (create a back buffer to render to)
	DXGI_SWAP_CHAIN_DESC sd;         // Structure to contain all the information needed
	ZeroMemory( &sd, sizeof( sd ) ); // Clear the structure to 0 - common Microsoft practice, not really good style
	sd.BufferCount = 1;
	sd.BufferDesc.Width = g_ViewportWidth;             // Target window size
	sd.BufferDesc.Height = g_ViewportHeight;           // --"--
	sd.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM; // Pixel format of target window
	sd.BufferDesc.RefreshRate.Numerator = 60;          // Refresh rate of monitor
	sd.BufferDesc.RefreshRate.Denominator = 1;         // --"--
	sd.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
	sd.SampleDesc.Count = 1;
	sd.SampleDesc.Quality = 0;
	sd.OutputWindow = hWnd;                            // Target window
	sd.Windowed = TRUE;                                // Whether to render in a window (TRUE) or go fullscreen (FALSE)
	UINT flags = 0;
	hr = D3D10CreateDeviceAndSwapChain( NULL, D3D10_DRIVER_TYPE_HARDWARE, NULL, flags,
										D3D10_SDK_VERSION, &sd, &SwapChain, &g_pd3dDevice );
	if( FAILED( hr ) ) return false;


	// Specify the render target as the back-buffer - this is an advanced topic. This code almost always occurs in the standard D3D setup
	ID3D10Texture2D* pBackBuffer;
	hr = SwapChain->GetBuffer( 0, __uuidof( ID3D10Texture2D ), ( LPVOID* )&pBackBuffer );
	if( FAILED( hr ) ) return false;
	hr = g_pd3dDevice->CreateRenderTargetView( pBackBuffer, NULL, &RenderTargetView );
	pBackBuffer->Release();
	if( FAILED( hr ) ) return false;


	// Create a texture (bitmap) to use for a depth buffer
	D3D10_TEXTURE2D_DESC descDepth;
	descDepth.Width = g_ViewportWidth;
	descDepth.Height = g_ViewportHeight;
	descDepth.MipLevels = 1;
	descDepth.ArraySize = 1;
	descDepth.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
	descDepth.SampleDesc.Count = 1;
	descDepth.SampleDesc.Quality = 0;
	descDepth.Usage = D3D10_USAGE_DEFAULT;
	descDepth.BindFlags = D3D10_BIND_DEPTH_STENCIL;
	descDepth.CPUAccessFlags = 0;
	descDepth.MiscFlags = 0;
	hr = g_pd3dDevice->CreateTexture2D( &descDepth, NULL, &DepthStencil );
	if( FAILED( hr ) ) return false;

	// Create the depth stencil view, i.e. indicate that the texture just created is to be used as a depth buffer
	D3D10_DEPTH_STENCIL_VIEW_DESC descDSV;
	descDSV.Format = descDepth.Format;
	descDSV.ViewDimension = D3D10_DSV_DIMENSION_TEXTURE2D;
	descDSV.Texture2D.MipSlice = 0;
	hr = g_pd3dDevice->CreateDepthStencilView( DepthStencil, &descDSV, &DepthStencilView );
	if( FAILED( hr ) ) return false;

	// Select the back buffer and depth buffer to use for rendering now
	g_pd3dDevice->OMSetRenderTargets( 1, &RenderTargetView, DepthStencilView );


	// Setup the viewport - defines which part of the window we will render to, almost always the whole window
	D3D10_VIEWPORT vp;
	vp.Width  = g_ViewportWidth;
	vp.Height = g_ViewportHeight;
	vp.MinDepth = 0.0f;
	vp.MaxDepth = 1.0f;
	vp.TopLeftX = 0;
	vp.TopLeftY = 0;
	g_pd3dDevice->RSSetViewports( 1, &vp );

	return true;
}

// Release the memory held by all objects created
void ReleaseResources()
{
	// The D3D setup and preparation of the geometry created several objects that use up memory (e.g. textures, vertex/index buffers etc.)
	// Each object that allocates memory (or hardware resources) needs to be "released" when we exit the program
	// There is similar code in every D3D program, but the list of objects that need to be released depends on what was created
	// Test each variable to see if it exists before deletion
	if( g_pd3dDevice )     g_pd3dDevice->ClearState();

	for (int i = 0; i < MAX_LIGHTS; i++) 
		delete Light[i];
	delete Floor;
	delete Cube;
	delete Sphere;
	delete Camera;
	delete Teapot;
	delete Hills;
	delete Troll;
	delete SkyBox;
	delete Ball;
	delete Torch;
	delete CutOut;
	delete Mirror;
	delete Portal;

    if( FloorDiffuseMap )	FloorDiffuseMap->Release();
	if( FloorNormalMap )	FloorNormalMap->Release();
	if( SphereDiffuseMap )	SphereDiffuseMap->Release();
    if( CubeDiffuseMap )	CubeDiffuseMap->Release();
	if( CubeNormalMap )		CubeNormalMap->Release();
	if( TeapotDiffuseMap )	TeapotDiffuseMap->Release();
	if( TeapotNormalMap )	TeapotNormalMap->Release();
	if( HillsDiffuseMap )	HillsDiffuseMap->Release();
	if( HillsNormalMap )	HillsNormalMap->Release();
	if( TrollDiffuseMap )	TrollDiffuseMap->Release();
	if( TorchDiffuseMap )	TorchDiffuseMap->Release();
	if( CubeMapDiffuseMap )	CubeMapDiffuseMap->Release();
	if( CutOutDiffuseMap )	CutOutDiffuseMap->Release();
	if( LightDiffuseMap )	LightDiffuseMap->Release();

	if( Effect )			Effect->Release();
	if( DepthStencilView )	DepthStencilView->Release();
	if( RenderTargetView )	RenderTargetView->Release();
	if( DepthStencil )		DepthStencil->Release();
	if( SwapChain )			SwapChain->Release();
	if( g_pd3dDevice )		g_pd3dDevice->Release();

	if (PortalDepthStencilView)  PortalDepthStencilView->Release();
	if (PortalDepthStencil)      PortalDepthStencil->Release();
	if (PortalMap)               PortalMap->Release();
	if (PortalRenderTarget)      PortalRenderTarget->Release();
	if (PortalTexture)           PortalTexture->Release();

}

void RefreshResources()
{
	for (int i = 0; i < MAX_LIGHTS; i++) 
		delete Light[i];
	delete Floor;
	delete Cube;
	delete Sphere;
	delete Teapot;
	delete Troll;
	delete SkyBox;
	delete Ball;
	delete Torch;
	delete CutOut;
	delete Mirror;
	delete Portal;

    if( FloorDiffuseMap )	FloorDiffuseMap->Release();
	if( FloorNormalMap )	FloorNormalMap->Release();
	if( SphereDiffuseMap )	SphereDiffuseMap->Release();
    if( CubeDiffuseMap )	CubeDiffuseMap->Release();
	if( CubeNormalMap )		CubeNormalMap->Release();
	if( TeapotDiffuseMap )	TeapotDiffuseMap->Release();
	if( TeapotNormalMap )	TeapotNormalMap->Release();
	if( HillsDiffuseMap )	HillsDiffuseMap->Release();
	if( HillsNormalMap )	HillsNormalMap->Release();
	if( TrollDiffuseMap )	TrollDiffuseMap->Release();
	if( TorchDiffuseMap )	TorchDiffuseMap->Release();
	if( CubeMapDiffuseMap )	CubeMapDiffuseMap->Release();
	if( CutOutDiffuseMap )	CutOutDiffuseMap->Release();
	if( LightDiffuseMap )	LightDiffuseMap->Release();

	if (PortalDepthStencilView)  PortalDepthStencilView->Release();
	if (PortalDepthStencil)      PortalDepthStencil->Release();
	if (PortalMap)               PortalMap->Release();
	if (PortalRenderTarget)      PortalRenderTarget->Release();
	if (PortalTexture)           PortalTexture->Release();
}

//--------------------------------------------------------------------------------------
// Load and compile Effect file (.fx file containing shaders)
//--------------------------------------------------------------------------------------
// An effect file contains a set of "Techniques". A technique is a combination of vertex, geometry and pixel shaders (and some states) used for
// rendering in a particular way. We load the effect file at runtime (it's written in HLSL and has the extension ".fx"). The effect code is compiled
// *at runtime* into low-level GPU language. When rendering a particular model we specify which technique from the effect file that it will use
//
bool LoadEffectFile()
{
	ID3D10Blob* pErrors; // This strangely typed variable collects any errors when compiling the effect file
	DWORD dwShaderFlags = D3D10_SHADER_ENABLE_STRICTNESS; // These "flags" are used to set the compiler options

	// Load and compile the effect file
	HRESULT hr = D3DX10CreateEffectFromFile( L"GraphicsAssign1.fx", NULL, NULL, "fx_4_0", dwShaderFlags, 0, g_pd3dDevice, NULL, NULL, &Effect, &pErrors, NULL );
	if( FAILED( hr ) )
	{
		if (pErrors != 0)  MessageBox( NULL, CA2CT(reinterpret_cast<char*>(pErrors->GetBufferPointer())), L"Error", MB_OK ); // Compiler error: display error message
		else               MessageBox( NULL, L"Error loading FX file. Ensure your FX file is in the same folder as this executable.", L"Error", MB_OK );  // No error message - probably file not found
		return false;
	}

	// Now we can select techniques from the compiled effect file
	PlainColourTechnique		= Effect->GetTechniqueByName( "PlainColour" );
	TexturedTechnique			= Effect->GetTechniqueByName( "Textured" );
	MovingTechnique				= Effect->GetTechniqueByName( "MovingTexture" );
	DiffuseSpecularTechnique	= Effect->GetTechniqueByName( "DiffuseSpecular" );
	NormalMappingTechnique		= Effect->GetTechniqueByName( "NormalMapping" );
	ParallaxMappingTechnique	= Effect->GetTechniqueByName( "ParallaxMapping" );
	CellShadingTechnique		= Effect->GetTechniqueByName( "CellShading" );
	CubeMapTechnique			= Effect->GetTechniqueByName( "CubeMap" );
	ReflectionTechnique			= Effect->GetTechniqueByName( "Reflection" );
	DepthOnlyTechnique			= Effect->GetTechniqueByName( "DepthOnly" );
	CutoutTechnique				= Effect->GetTechniqueByName( "Cutout" );
	MirrorTechnique				= Effect->GetTechniqueByName( "Mirror" );
	MirrorSurfaceTechnique		= Effect->GetTechniqueByName( "MirrorSurface" );
	AdditiveTechnique			= Effect->GetTechniqueByName( "Additive" );

	// Create special variables to allow us to access global variables in the shaders from C++
	WorldMatrixVar    = Effect->GetVariableByName( "WorldMatrix" )->AsMatrix();
	ViewMatrixVar     = Effect->GetVariableByName( "ViewMatrix"  )->AsMatrix();
	ProjMatrixVar     = Effect->GetVariableByName( "ProjMatrix"  )->AsMatrix();
	InverseMatrixVar    = Effect->GetVariableByName( "WorldInverseTranspose" )->AsMatrix();

	// We access the texture variable in the shader in the same way as we have before for matrices, light data etc.
	// Only difference is that this variable is a "Shader Resource"
	DiffuseMapVar	= Effect->GetVariableByName( "DiffuseMap" )->AsShaderResource();
	NormalMapVar	= Effect->GetVariableByName( "NormalMap" )->AsShaderResource();
	CellMapVar		= Effect->GetVariableByName( "CellMap" )->AsShaderResource();
	ShadowMap1Var	= Effect->GetVariableByName( "ShadowMap1" )->AsShaderResource();
	ShadowMap2Var	= Effect->GetVariableByName( "ShadowMap2" )->AsShaderResource();
	ShadowMap3Var	= Effect->GetVariableByName( "ShadowMap3" )->AsShaderResource();
	CubeMapVar		= Effect->GetVariableByName( "CubeMapTexture" )->AsShaderResource();

	// Other shader variables
	ModelColourVar = Effect->GetVariableByName( "ModelColour"  )->AsVector();

	//Added by me
	Tint				= Effect->GetVariableByName( "Tint"				)->AsVector();
	Amount				= Effect->GetVariableByName( "Amount"			)->AsScalar();
	AttenuationVar		= Effect->GetVariableByName( "attenuation"		)->AsScalar();
	CameraPosVar		= Effect->GetVariableByName( "cameraPos"		)->AsVector();
	AmbientColourVar	= Effect->GetVariableByName( "ambientColour"	)->AsVector();
	SpecularPowerVar	= Effect->GetVariableByName( "specularPower"	)->AsScalar();
	ParallaxDepthVar	= Effect->GetVariableByName( "ParallaxDepth"	)->AsScalar();
	OutlineThicknessVar = Effect->GetVariableByName( "outlineThickness"	)->AsScalar();
	ClipPlaneVar		= Effect->GetVariableByName( "ClipPlane"		)->AsVector();
	
	return true;
}



//--------------------------------------------------------------------------------------
// Scene Setup / Update / Rendering
//--------------------------------------------------------------------------------------

// Create / load the camera, models and textures for the scene
void SetTextureDescription(D3D10_TEXTURE2D_DESC &texDesc)
{
	texDesc.Width  = ShadowMapSize;
	texDesc.Height = ShadowMapSize;
	texDesc.MipLevels = 1;
	texDesc.ArraySize = 1;
	texDesc.Format = DXGI_FORMAT_R32_TYPELESS;
	texDesc.SampleDesc.Count = 1;
	texDesc.SampleDesc.Quality = 0;
	texDesc.Usage = D3D10_USAGE_DEFAULT;
	texDesc.BindFlags = D3D10_BIND_DEPTH_STENCIL | D3D10_BIND_SHADER_RESOURCE;
	texDesc.CPUAccessFlags = 0;
	texDesc.MiscFlags = 0;
}

bool InitScene()
{
	//////////////////
	// Create camera

	if(!Camera)
	{
		Camera = new CCamera();
		Camera->SetPosition( D3DXVECTOR3(-15, 15,-40) );
		Camera->SetRotation( D3DXVECTOR3(0.0f,40.85f,0.0f) ); // ToRadians is a new helper function to convert degrees to radians
	}
		PortalCamera = new CCamera();
		PortalCamera->SetPosition( D3DXVECTOR3(-15, 20,-40) );
		PortalCamera->SetRotation( D3DXVECTOR3(ToRadians(13.0f), ToRadians(18.0f), 0.0f) );


	///////////////////////
	// Load/Create models

	Cube = new CModel;
	Floor = new CModel;

	Sphere = new CModel;
	Teapot = new CModel;
	Hills = new CModel;
	Troll = new CModel;
	SkyBox = new CModel;
	Ball = new CModel;
	Grenade = new CModel;
	Torch = new CModel;
	CutOut = new CModel;
	Mirror = new CModel;
	Portal = new CModel;

	switch(technique)
	{
	case 0:
		currentTechnique = PlainColourTechnique;
		tangents = false;
		break;
	case 1:
		currentTechnique = TexturedTechnique;
		tangents = false;
		break;
	case 2:
		currentTechnique = DiffuseSpecularTechnique;
		tangents = false;
		break;
	case 3:
		currentTechnique = NormalMappingTechnique;
		tangents = true;
		break;
	case 4:
		currentTechnique = ParallaxMappingTechnique;
		tangents = true;
		break;
	default:
	currentTechnique = DiffuseSpecularTechnique;
	};

	// The model class can load ".X" files. It encapsulates (i.e. hides away from this code) the file loading/parsing and creation of vertex/index buffers
	// We must pass an example technique used for each model. We can then only render models with techniques that uses matching vertex input data
	if (!Cube->  Load( "Cube.x",	currentTechnique,	tangents)) return false;
	if (!Floor-> Load( "Floor.x",	currentTechnique,	tangents)) return false;
	
	//My Models
	if (!Sphere->Load( "Sphere.x",	MovingTechnique				)) return false;
	if (!Teapot->Load( "Teapot.x",	currentTechnique,	tangents)) return false;
	if (!Hills->Load( "Hills.x",	currentTechnique,	tangents)) return false;
	if (!Troll-> Load( "Troll.x",	CellShadingTechnique		)) return false;
	if (!SkyBox->Load( "Cube.x",	DiffuseSpecularTechnique	)) return false;
	//if (!Ball->Load( "Sphere.x",	ReflectionTechnique, true	)) return false; //Lol what a failure
	if (!Grenade->Load( "Grenade.x",currentTechnique, true		)) return false;
	if (!Torch->Load( "Torch.x",	TexturedTechnique			)) return false;
	if (!CutOut->Load( "Cube.x",	CutoutTechnique, true		)) return false;
	if (!Mirror->Load( "Mirror.x",  MirrorTechnique				)) return false;
	if (!Portal->Load( "Portal.x",  TexturedTechnique    )) return false;
	
	//Load Lights
	CSpotLight* Spot1 = new CSpotLight(TexturedTechnique);
	CSpotLight* Spot2 = new CSpotLight(TexturedTechnique);
	CPointLight* Point1 = new CPointLight(CellShadingTechnique);

	Light[0] = Spot1;
	Light[0]->LinkToShader(Effect,"light1Pos","light1Colour","light1Facing","light1ViewMatrix","light1ProjMatrix","light1CosHalfAngle");
	
	Light[1] = Spot2;
	Light[1]->LinkToShader(Effect,"light2Pos","light2Colour","light2Facing","light2ViewMatrix","light2ProjMatrix","light2CosHalfAngle");

	Light[2] = Point1;
	Light[2]->LinkToShader(Effect,"light3Pos","light3Colour");

	// Initial positions
	Cube->SetPosition( D3DXVECTOR3(0, 10, 0) );
	Sphere->SetPosition(D3DXVECTOR3(0,100,100));
	Light[0]->SetScale( 0.1f ); // Nice if size of light reflects its brightness
	Light[0]->SetColour(Light1Colour);

	Light[1]->SetPosition( D3DXVECTOR3(-20, 30, 50) );
	Light[1]->SetScale( 0.2f );
	Light[1]->SetColour(Light2Colour);

	Light[2]->SetPosition( D3DXVECTOR3(100, 50, 500) );
	Light[2]->SetScale( 10.0f );
	Light[2]->SetColour(Light3Colour);

	Torch->SetPosition( D3DXVECTOR3(-20, 10, 50) );
	//Torch->SetScale( 1.0f );
	Teapot->SetPosition( D3DXVECTOR3(30,1,50));
	Grenade->SetPosition( D3DXVECTOR3(30,30,50));
	Hills->SetPosition(D3DXVECTOR3(100,3,500));
	Troll->SetPosition( D3DXVECTOR3(-30, 1, 0) );
	Troll->SetScale( 4.0f );
	SkyBox->SetPosition( D3DXVECTOR3(0, 990, 0) );
	SkyBox->SetScale( 200.0f );
	Ball->SetPosition( D3DXVECTOR3(200, 80, 0) );
	CutOut->SetPosition( D3DXVECTOR3(30, 10, 0));

	Mirror->SetPosition( D3DXVECTOR3(0, 20, 30) );
	Mirror->SetRotation( D3DXVECTOR3(0.0f, ToRadians(-180.0f), 0.0f) );

	Portal->SetPosition( D3DXVECTOR3(-15, 15,-75) );
	Portal->SetRotation( D3DXVECTOR3(0.0f, ToRadians(0.0f), 0.0f) );


	//////////////////
	// Load textures

	if (FAILED( D3DX10CreateShaderResourceViewFromFile( g_pd3dDevice, L"CobbleDiffuseSpecular.dds",	NULL, NULL, &CubeDiffuseMap,	NULL ) )) return false;
	if (FAILED( D3DX10CreateShaderResourceViewFromFile( g_pd3dDevice, L"CobbleNormalDepth.dds",		NULL, NULL, &CubeNormalMap,		NULL ) )) return false;

	if (FAILED( D3DX10CreateShaderResourceViewFromFile( g_pd3dDevice, L"WoodDiffuseSpecular.dds",	NULL, NULL, &FloorDiffuseMap,	NULL ) )) return false;
	if (FAILED( D3DX10CreateShaderResourceViewFromFile( g_pd3dDevice, L"WoodNormal.dds",			NULL, NULL, &FloorNormalMap,	NULL ) )) return false;

	if (FAILED( D3DX10CreateShaderResourceViewFromFile( g_pd3dDevice, L"PatternDiffuseSpecular.dds",NULL, NULL, &TeapotDiffuseMap,	NULL ) )) return false;
	if (FAILED( D3DX10CreateShaderResourceViewFromFile( g_pd3dDevice, L"PatternNormalDepth.dds",	NULL, NULL, &TeapotNormalMap,	NULL ) )) return false;

	if (FAILED( D3DX10CreateShaderResourceViewFromFile( g_pd3dDevice, L"PatternDiffuseSpecular.dds",NULL, NULL, &HillsDiffuseMap,	NULL ) )) return false;
	if (FAILED( D3DX10CreateShaderResourceViewFromFile( g_pd3dDevice, L"PatternNormalDepth.dds",	NULL, NULL, &HillsNormalMap,	NULL ) )) return false;

	if (FAILED( D3DX10CreateShaderResourceViewFromFile( g_pd3dDevice, L"PatternDiffuseSpecular.dds",NULL, NULL, &GrenadeDiffuseMap, NULL ) )) return false;
	if (FAILED( D3DX10CreateShaderResourceViewFromFile( g_pd3dDevice, L"PatternNormalDepth.dds",	NULL, NULL, &GrenadeNormalMap,	NULL ) )) return false;

	if (FAILED( D3DX10CreateShaderResourceViewFromFile( g_pd3dDevice, L"Moon.jpg",					NULL, NULL, &SphereDiffuseMap,	NULL ) )) return false;

	if (FAILED( D3DX10CreateShaderResourceViewFromFile( g_pd3dDevice, L"Green.png",					NULL, NULL, &TrollDiffuseMap,	NULL ) )) return false;
	if (FAILED( D3DX10CreateShaderResourceViewFromFile( g_pd3dDevice, L"CellGradient.png",			NULL, NULL, &CellMap,			NULL ) )) return false;
	if (FAILED( D3DX10CreateShaderResourceViewFromFile( g_pd3dDevice, L"Green.png",					NULL, NULL, &TorchDiffuseMap,	NULL ) )) return false;

	if (FAILED( D3DX10CreateShaderResourceViewFromFile( g_pd3dDevice, L"tiles1.jpg",				NULL, NULL, &CubeMapDiffuseMap,	NULL ) )) return false;

	if (FAILED( D3DX10CreateShaderResourceViewFromFile( g_pd3dDevice, L"Moogle.png",				NULL, NULL, &CutOutDiffuseMap,	NULL ) )) return false;

	if (FAILED( D3DX10CreateShaderResourceViewFromFile( g_pd3dDevice, L"flare.jpg",					NULL, NULL, &LightDiffuseMap,  NULL ) )) return false;



	/////////////////////////
	//Shadow Setup
	D3D10_TEXTURE2D_DESC texDesc;
	SetTextureDescription(texDesc);
	if (FAILED( g_pd3dDevice->CreateTexture2D( &texDesc, NULL, &ShadowMap1Texture ) )) return false;
	if (FAILED( g_pd3dDevice->CreateTexture2D( &texDesc, NULL, &ShadowMap2Texture ) )) return false;
	if (FAILED( g_pd3dDevice->CreateTexture2D( &texDesc, NULL, &ShadowMap3Texture ) )) return false;

	D3D10_DEPTH_STENCIL_VIEW_DESC descDSV;
	descDSV.Format = DXGI_FORMAT_D32_FLOAT;
	descDSV.ViewDimension = D3D10_DSV_DIMENSION_TEXTURE2D;
	descDSV.Texture2D.MipSlice = 0;
	if (FAILED( g_pd3dDevice->CreateDepthStencilView( ShadowMap1Texture, &descDSV, &ShadowMap1DepthView ) )) return false;
	if (FAILED( g_pd3dDevice->CreateDepthStencilView( ShadowMap2Texture, &descDSV, &ShadowMap2DepthView ) )) return false;
	if (FAILED( g_pd3dDevice->CreateDepthStencilView( ShadowMap3Texture, &descDSV, &ShadowMap3DepthView ) )) return false;


	D3D10_SHADER_RESOURCE_VIEW_DESC srDesc;
	srDesc.Format = DXGI_FORMAT_R32_FLOAT;
	srDesc.ViewDimension = D3D10_SRV_DIMENSION_TEXTURE2D;
	srDesc.Texture2D.MostDetailedMip = 0;
	srDesc.Texture2D.MipLevels = 1;
	if (FAILED( g_pd3dDevice->CreateShaderResourceView( ShadowMap1Texture, &srDesc, &ShadowMap1 ) )) return false;
	if (FAILED( g_pd3dDevice->CreateShaderResourceView( ShadowMap2Texture, &srDesc, &ShadowMap2 ) )) return false;
	if (FAILED( g_pd3dDevice->CreateShaderResourceView( ShadowMap3Texture, &srDesc, &ShadowMap3) )) return false;

	/////////////////////////
	//CubeMap - Doesnt work
	/*D3DX10_IMAGE_LOAD_INFO loadSMInfo;
	loadSMInfo.MiscFlags = D3D10_RESOURCE_MISC_TEXTURECUBE;
	ID3D10Texture2D* SMTexture = 0;

	D3D10_TEXTURE2D_DESC SMTextureDesc;
	SMTexture->GetDesc(&SMTextureDesc);

	D3D10_SHADER_RESOURCE_VIEW_DESC SMViewDesc;
	SMViewDesc.Format = SMTextureDesc.Format;
	SMViewDesc.ViewDimension = D3D10_SRV_DIMENSION_TEXTURECUBE;
	SMViewDesc.TextureCube.MipLevels = SMTextureDesc.MipLevels;
	SMViewDesc.TextureCube.MostDetailedMip = 0;*/

	//if (FAILED(d3dDevice->CreateShaderResourceView(SMTexture, &SMViewDesc, &smrv))) return false;
	//if (FAILED( D3DX10CreateShaderResourceViewFromFile( g_pd3dDevice, L"CubeMap.dds", &loadSMInfo, NULL, (ID3D10Resource**)&SMTexture, 0) )) return false;

	////////////////////////////
	//Portal Setup
	D3D10_TEXTURE2D_DESC portalDesc;
	portalDesc.Width  = PortalWidth;  // Size of the portal texture determines its quality
	portalDesc.Height = PortalHeight;
	portalDesc.MipLevels = 1; // No mip-maps when rendering to textures (or we would have to render every level)
	portalDesc.ArraySize = 1;
	portalDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM; // RGBA texture (8-bits each)
	portalDesc.SampleDesc.Count = 1;
	portalDesc.SampleDesc.Quality = 0;
	portalDesc.Usage = D3D10_USAGE_DEFAULT;
	portalDesc.BindFlags = D3D10_BIND_RENDER_TARGET | D3D10_BIND_SHADER_RESOURCE; // Indicate we will use texture as render target, and pass it to shaders
	portalDesc.CPUAccessFlags = 0;
	portalDesc.MiscFlags = 0;
	if (FAILED( g_pd3dDevice->CreateTexture2D( &portalDesc, NULL, &PortalTexture ) )) return false;

	// We created the portal texture above, now we get a "view" of it as a render target, i.e. get a special pointer to the texture that
	// we use when rendering to it (see RenderScene function below)
	if (FAILED( g_pd3dDevice->CreateRenderTargetView( PortalTexture, NULL, &PortalRenderTarget ) )) return false;

	// We also need to send this texture (resource) to the shaders. To do that we must create a shader-resource "view"
	srDesc.Format = portalDesc.Format;
	srDesc.ViewDimension = D3D10_SRV_DIMENSION_TEXTURE2D;
	srDesc.Texture2D.MostDetailedMip = 0;
	srDesc.Texture2D.MipLevels = 1;
	if (FAILED( g_pd3dDevice->CreateShaderResourceView( PortalTexture, &srDesc, &PortalMap ) )) return false;


	//**** Portal Depth Buffer ****//

	// We also need a depth buffer to go with our portal
	//**** This depth buffer can be shared with any other portals of the same size
	portalDesc.Width  = PortalWidth;
	portalDesc.Height = PortalHeight;
	portalDesc.MipLevels = 1;
	portalDesc.ArraySize = 1;
	portalDesc.Format = DXGI_FORMAT_D32_FLOAT;
	portalDesc.SampleDesc.Count = 1;
	portalDesc.SampleDesc.Quality = 0;
	portalDesc.Usage = D3D10_USAGE_DEFAULT;
	portalDesc.BindFlags = D3D10_BIND_DEPTH_STENCIL;
	portalDesc.CPUAccessFlags = 0;
	portalDesc.MiscFlags = 0;
	if (FAILED(g_pd3dDevice->CreateTexture2D( &portalDesc, NULL, &PortalDepthStencil ) )) return false;

	// Create the depth stencil view, i.e. indicate that the texture just created is to be used as a depth buffer
	D3D10_DEPTH_STENCIL_VIEW_DESC portalDescDSV;
	portalDescDSV.Format = portalDesc.Format;
	portalDescDSV.ViewDimension = D3D10_DSV_DIMENSION_TEXTURE2D;
	portalDescDSV.Texture2D.MipSlice = 0;
	if (FAILED(g_pd3dDevice->CreateDepthStencilView( PortalDepthStencil, &portalDescDSV, &PortalDepthStencilView ) )) return false;

	return true;
}

// Update the scene - move/rotate each model and the camera, then update their matrices
void UpdateScene( float frameTime )
{
	// Control camera position and update its matrices (view matrix, projection matrix) each frame
	// Don't be deceived into thinking that this is a new method to control models - the same code we used previously is in the camera class
	Camera->Control( frameTime, Key_Up, Key_Down, Key_Left, Key_Right, Key_W, Key_S, Key_A, Key_D , 50.0f);
	Camera->UpdateMatrices();

	PortalCamera->UpdateMatrices();
	
	// Control cube position and update its world matrix each frame
	Cube->Control( frameTime, Key_I, Key_K, Key_J, Key_L, Key_U, Key_O, Key_Period, Key_Comma );
	Cube->UpdateMatrix();

	//Update wiggle
	wiggle += 2.0f * frameTime;
	Amount->SetFloat(wiggle);
	Amount = Effect->GetVariableByName( "Amount" )->AsScalar();
	Sphere->UpdateMatrix();

	// Update the orbiting light - a bit of a cheat with the static variable [ask the tutor if you want to know what this is]
	static float Rotate = 0.0f;
	Light[0]->SetPosition( Cube->GetPosition() + D3DXVECTOR3(cos(Rotate)*LightOrbitRadius, 10, sin(Rotate)*LightOrbitRadius) );
	Rotate -= LightOrbitSpeed * frameTime;
	Light[0]->FacePoint( Cube->GetPosition() );
	Light[0]->MorphColour(wiggle);

	Light[1]->FacePoint( Cube->GetPosition() );
	Light[1]->MorphBrightness(wiggle);

	Light[2]->MorphColour(wiggle);
	
	Light[0]->UpdateMatrix();
	Light[1]->UpdateMatrix();
	Light[2]->UpdateMatrix();

	//Update Teapot
	Teapot->UpdateMatrix();
	Hills->UpdateMatrix();
	Troll->UpdateMatrix();
	SkyBox->UpdateMatrix();
	Ball->UpdateMatrix();
	Grenade->UpdateMatrix();
	Torch->UpdateMatrix();
	CutOut->UpdateMatrix();
	Mirror->UpdateMatrix();
	Portal->UpdateMatrix();

	if(KeyHit(Key_1)) {technique = PlainColour;		RefreshResources(); InitScene();}
	if(KeyHit(Key_2)) {technique = Textured;		RefreshResources(); InitScene();}
	if(KeyHit(Key_3)) {technique = DiffuseSpecular; RefreshResources(); InitScene();}
	if(KeyHit(Key_4)) {technique = NormalMapping;	RefreshResources(); InitScene();}
	if(KeyHit(Key_5)) {technique = ParallaxMapping; RefreshResources(); InitScene();}
}

void RenderMain( CCamera* camera )
{
	// Pass the camera's matrices to the vertex shader and position to the vertex shader
	ViewMatrixVar->SetMatrix( camera->GetViewMatrix() );
	ProjMatrixVar->SetMatrix( camera->GetProjectionMatrix() );
	CameraPosVar->SetRawValue( camera->GetPosition(), 0, 12 );


	// Send the shadow maps rendered in the RenderShadowMap function to the shader to use for shadow testing
    ShadowMap1Var->SetResource( ShadowMap1 );
	ShadowMap2Var->SetResource( ShadowMap2 );
  

	D3DXVECTOR3 Black( 0.0f, 0.0f, 0.0f );
	D3DXVECTOR3 Blue( 0.0f, 0.0f, 1.0f );
	D3DXVECTOR3 Red( 1.0f, 0.0f, 0.0f );

	Tint->SetRawValue(Blue, 0, 12);

	// Render cube
	WorldMatrixVar->SetMatrix( (float*)Cube->GetWorldMatrix() );
    DiffuseMapVar->SetResource( CubeDiffuseMap );
	NormalMapVar->SetResource( CubeNormalMap );
	ModelColourVar->SetRawValue( Blue, 0, 12 );
	Cube->Render( currentTechnique );

	// Same for the other models in the scene
	WorldMatrixVar->SetMatrix( (float*)Floor->GetWorldMatrix() );
    DiffuseMapVar->SetResource( FloorDiffuseMap );
	NormalMapVar->SetResource( FloorNormalMap );
	ModelColourVar->SetRawValue( Black, 0, 12 );
	Floor->Render( currentTechnique );

	WorldMatrixVar->SetMatrix( (float*)Sphere->GetWorldMatrix() );
    DiffuseMapVar->SetResource( SphereDiffuseMap );
	ModelColourVar->SetRawValue( Red, 0, 12 );
	Sphere->Render( MovingTechnique );

	WorldMatrixVar->SetMatrix( (float*)Teapot->GetWorldMatrix() );
    DiffuseMapVar->SetResource( TeapotDiffuseMap );
	NormalMapVar->SetResource( TeapotNormalMap );
	ModelColourVar->SetRawValue( Red, 0, 12 );
	Teapot->Render( currentTechnique );

	WorldMatrixVar->SetMatrix( (float*)Hills->GetWorldMatrix() );
    DiffuseMapVar->SetResource( HillsDiffuseMap );
	NormalMapVar->SetResource( HillsNormalMap );
	ModelColourVar->SetRawValue( Red, 0, 12 );
	Hills->Render( currentTechnique );

	WorldMatrixVar->SetMatrix( (float*)Grenade->GetWorldMatrix() );
    DiffuseMapVar->SetResource( GrenadeDiffuseMap );
	NormalMapVar->SetResource( GrenadeNormalMap );
	ModelColourVar->SetRawValue( Red, 0, 12 );
	Grenade->Render( currentTechnique );

	WorldMatrixVar->SetMatrix( (float*)Troll->GetWorldMatrix() );
	ModelColourVar->SetRawValue( OutlineColour, 0, 12 );
    DiffuseMapVar->SetResource( TrollDiffuseMap );
	Troll->Render( CellShadingTechnique ); 

	WorldMatrixVar->SetMatrix( (float*)SkyBox->GetWorldMatrix() );
	DiffuseMapVar->SetResource( CubeMapDiffuseMap );
	ModelColourVar->SetRawValue( Red, 0, 12 );
	SkyBox->Render( DiffuseSpecularTechnique );

	WorldMatrixVar->SetMatrix( (float*)CutOut->GetWorldMatrix() );
	DiffuseMapVar->SetResource( CutOutDiffuseMap );
	ModelColourVar->SetRawValue( Red, 0, 12 );
	CutOut->Render( CutoutTechnique );
	
	//Lights
	for (int i = 0; i < MAX_LIGHTS; i++)
	{
		WorldMatrixVar->SetMatrix( (float*)Light[i]->GetWorldMatrix() );
		ModelColourVar->SetRawValue( Light[i]->GetColour(), 0, 12 );
		if(i==2)
		{
			DiffuseMapVar->SetResource( LightDiffuseMap );
			Light[i]->Render( AdditiveTechnique );
		}
		else
			Light[i]->Render( PlainColourTechnique );
	}

	WorldMatrixVar->SetMatrix( (float*)Torch->GetWorldMatrix() );
	DiffuseMapVar->SetResource( TorchDiffuseMap );
	ModelColourVar->SetRawValue( Red, 0, 12 );
	Torch->Render( CellShadingTechnique );

	AmbientColourVar->SetRawValue( AmbientColour, 0, 12 );
	SpecularPowerVar->SetFloat(SpecularPower);
	AttenuationVar->SetFloat(Attenuation);
	ParallaxDepthVar->SetFloat(ParallaxDepth);

	CellMapVar->SetResource( CellMap );
	OutlineThicknessVar->SetFloat( OutlineThickness );

	WorldMatrixVar->SetMatrix( (float*)Portal->GetWorldMatrix() );
    DiffuseMapVar->SetResource( PortalMap );
	Portal->Render( TexturedTechnique ); 
}

D3DXMATRIXA16 CalculateInverseMatrix( CModel* model )
{
	D3DXMATRIXA16 viewMatrix;
	
	D3DXMATRIXA16 worldMatrix = model->GetWorldMatrix();
	D3DXMatrixInverse( &viewMatrix, NULL, &worldMatrix );

	return viewMatrix;
}
D3DXMATRIXA16 CalculateLightViewMatrix( CLight* Light )
{
	D3DXMATRIXA16 viewMatrix;
	
	D3DXMATRIXA16 worldMatrix = Light->GetWorldMatrix();
	D3DXMatrixInverse( &viewMatrix, NULL, &worldMatrix );

	return viewMatrix;
}
D3DXMATRIXA16 CalculateLightProjMatrix()
{
	D3DXMATRIXA16 projMatrix;

	// Create a projection matrix for the light. Use the spotlight cone angle as an FOV, just set default values for everything else.
	D3DXMatrixPerspectiveFovLH( &projMatrix, ToRadians( SpotlightConeAngle ), 1, 1.0f, 10000.0f);

	return projMatrix;
}

void RenderShadowMap( CLight* light )
{
	//---------------------------------
	// Set "camera" matrices in shader
	// Pass the light's "camera" matrices to the vertex shader - use helper functions above to turn spotlight settings into "camera" matrices
	ViewMatrixVar->SetMatrix( CalculateLightViewMatrix( light ) );
	ProjMatrixVar->SetMatrix( CalculateLightProjMatrix(  ) );
	

	//-----------------------------------
	// Render each model into shadow map

	WorldMatrixVar->SetMatrix( Cube->GetWorldMatrix() );
	Cube->Render( DepthOnlyTechnique );

	WorldMatrixVar->SetMatrix( CutOut->GetWorldMatrix() );
	CutOut->Render( DepthOnlyTechnique );

	WorldMatrixVar->SetMatrix( Floor->GetWorldMatrix() );
	Floor->Render( DepthOnlyTechnique );

	WorldMatrixVar->SetMatrix( Troll->GetWorldMatrix() );
	Troll->Render( DepthOnlyTechnique );

	WorldMatrixVar->SetMatrix( Teapot->GetWorldMatrix() );
	Teapot->Render( DepthOnlyTechnique );
}

void RenderMirrors(CCamera* camera)
{
	//*******************************************************************************************************
	// First set the stencil value for all mirror pixels to 1 and clear the mirror to a fixed colour and 
	// set its depth-buffer values to maximum (so we can render "inside" the mirror)

	D3DXVECTOR3 Grey( 0.3f, 0.3f, 0.3f );

	D3DXMATRIXA16 mirrorMatrix = Mirror->GetWorldMatrix();
	WorldMatrixVar->SetMatrix( (float*)mirrorMatrix );
	ModelColourVar->SetRawValue( Grey, 0, 12 );
	Mirror->Render( MirrorTechnique );


	//*******************************************************************************************************
	// Next reflect the camera in the mirror

	// Some mathematics to get as reflected version of the camera - using DirectX helper functions mostly

	// Create a plane for the mirror
	D3DXPLANE mirrorPlane;
	D3DXVECTOR3 mirrorPoint  = D3DXVECTOR3( mirrorMatrix(3,0), mirrorMatrix(3,1), mirrorMatrix(3,2) );
	D3DXVECTOR3 mirrorNormal = D3DXVECTOR3( mirrorMatrix(2,0), mirrorMatrix(2,1), mirrorMatrix(2,2) );
	D3DXPlaneFromPointNormal( &mirrorPlane, &mirrorPoint, &mirrorNormal );
	
	// Reflect the camera's view matrix in the mirror plane
	D3DXMATRIXA16 reflectMatrix;
	D3DXMatrixReflect( &reflectMatrix, &mirrorPlane ); //Don't know why this crashed, but it does
	D3DXMATRIXA16 reflectViewMatrix = reflectMatrix * Camera->GetViewMatrix();

	// Reflect the camera's position in the mirror plane
	D3DXVECTOR3 cameraPos = camera->GetPosition();
	D3DXVECTOR4 reflectCameraPos4; // Initially generate a 4 element vector
	D3DXVec3Transform( &reflectCameraPos4, &cameraPos, &reflectMatrix );
	D3DXVECTOR3 reflectCameraPos = D3DXVECTOR3( (float*)reflectCameraPos4 ); // Drop 4th element


	//*******************************************************************************************************
	// Render all the models "inside" the mirror

	// Set the reflected camera data in the shaders
	ViewMatrixVar->SetMatrix( (float*)&reflectViewMatrix );
	CameraPosVar->SetRawValue( reflectCameraPos, 0, 12 );
	ClipPlaneVar->SetRawValue( mirrorPlane, 0, 16 );

	// Need to use slightly different techniques to avoid mirror rendering being "inside out"
	RenderMain(camera);

	// Restore main camera data in the shaders
	ViewMatrixVar->SetMatrix( (float*)&camera->GetViewMatrix() );
	CameraPosVar->SetRawValue( camera->GetPosition(), 0, 12 );
	ClipPlaneVar->SetRawValue( D3DXVECTOR4(0,0,0,0), 0, 16 );


	//*******************************************************************************************************
	// Finally draw a "surface" for the mirror - a transparent layer over the mirror contents. This sets the correct depth-buffer values 
	// for the mirror surface, so further rendering won't go "inside" the mirrored scene

	WorldMatrixVar->SetMatrix( (float*)Mirror->GetWorldMatrix() );
	Mirror->Render( MirrorSurfaceTechnique );
}

// Render everything in the scene
void RenderScene()
{
	// Clear the back buffer - before drawing the geometry clear the entire window to a fixed colour
	float ClearColor[4] = { 0.2f, 0.2f, 0.3f, 1.0f }; // Good idea to match background to ambient colour
	g_pd3dDevice->ClearRenderTargetView( RenderTargetView, ClearColor );
	g_pd3dDevice->ClearDepthStencilView( DepthStencilView, D3D10_CLEAR_DEPTH | D3D10_CLEAR_STENCIL, 1.0f, 0 ); // Clear the depth buffer too


	//---------------------------
	// Common rendering settings

	// Common features for all models, set these once only

	//Pass Lighting Information
	Light[0]->SendToShader();
	Light[1]->SendToShader();
	Light[2]->SendToShader();

	// Pass the camera's matrices to the vertex shader
	ViewMatrixVar->SetMatrix( (float*)&Camera->GetViewMatrix() );
	ProjMatrixVar->SetMatrix( (float*)&Camera->GetProjectionMatrix() );
	CameraPosVar->SetRawValue( Camera->GetPosition(), 0, 12 );

	//---------------------------
	// Render shadow maps

	// Setup the viewport - defines which part of the shadow map we will render to (usually all of it)
	D3D10_VIEWPORT vp;
	vp.Width  = ShadowMapSize;
	vp.Height = ShadowMapSize;
	vp.MinDepth = 0.0f;
	vp.MaxDepth = 1.0f;
	vp.TopLeftX = 0;
	vp.TopLeftY = 0;
	g_pd3dDevice->RSSetViewports( 1, &vp );

	// Rendering a single shadow map for a light
	// 1. Select the shadow map texture as the current depth buffer. We will not be rendering any pixel colours
	// 2. Clear the shadow map texture (as a depth buffer)
	// 3. Render everything from point of view of light 0
	g_pd3dDevice->OMSetRenderTargets( 0, 0, ShadowMap1DepthView );
	g_pd3dDevice->ClearDepthStencilView( ShadowMap1DepthView, D3D10_CLEAR_DEPTH, 1.0f, 0 );
	RenderShadowMap( Light[0] );

	g_pd3dDevice->OMSetRenderTargets( 0, 0, ShadowMap2DepthView );
	g_pd3dDevice->ClearDepthStencilView( ShadowMap2DepthView, D3D10_CLEAR_DEPTH, 1.0f, 0 );
	RenderShadowMap( Light[1] );

	//---------------------------
	// Render Portal Scene
	vp;
	vp.Width  = PortalWidth;
	vp.Height = PortalHeight;
	vp.MinDepth = 0.0f;
	vp.MaxDepth = 1.0f;
	vp.TopLeftX = 0;
	vp.TopLeftY = 0;
	g_pd3dDevice->RSSetViewports( 1, &vp );

	// Select the portal texture to use for rendering, will share the depth/stencil buffer with the backbuffer though
	g_pd3dDevice->OMSetRenderTargets( 1, &PortalRenderTarget, PortalDepthStencilView );

	// Clear the portal texture and its depth buffer
	g_pd3dDevice->ClearRenderTargetView( PortalRenderTarget, &ClearColor[0] );
	g_pd3dDevice->ClearDepthStencilView( PortalDepthStencilView, D3D10_CLEAR_DEPTH, 1.0f, 0 );

	// Render everything from the portal camera's point of view (into the portal render target [texture] set above)
	RenderMain( PortalCamera );

	//---------------------------
	// Render main scene

	// Setup the viewport within - defines which part of the back-buffer we will render to (usually all of it)
	vp.Width  = g_ViewportWidth;
	vp.Height = g_ViewportHeight;
	vp.MinDepth = 0.0f;
	vp.MaxDepth = 1.0f;
	vp.TopLeftX = 0;
	vp.TopLeftY = 0;
	g_pd3dDevice->RSSetViewports( 1, &vp );

	// Select the back buffer and depth buffer to use for rendering
	g_pd3dDevice->OMSetRenderTargets( 1, &RenderTargetView, DepthStencilView );
	
	// Clear the back buffer and its depth buffer
	g_pd3dDevice->ClearRenderTargetView( RenderTargetView, &ClearColor[0] );
	g_pd3dDevice->ClearDepthStencilView( DepthStencilView, D3D10_CLEAR_DEPTH, 1.0f, 0 );

	// Render everything from the main camera's point of view

	//RenderMirrors(Camera);
	RenderMain( Camera );
	//RenderShadowMap( Light[0] );


	//---------------------------
	// Display the Scene

	// After we've finished drawing to the off-screen back buffer, we "present" it to the front buffer (the screen)
	SwapChain->Present( 0, 0 );
}
