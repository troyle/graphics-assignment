#ifndef LIGHT_H_INCLUDED
#define LIGHT_H_INCLUDED

#include <d3d10.h>
#include <d3dx10.h>
#include "Model.h"

class CLight
{
public:
	virtual void SendToShader() = 0;
	virtual void LinkToShader(ID3D10Effect* &Effect,LPCSTR Pos,LPCSTR Col,LPCSTR Fac=NULL,LPCSTR View=NULL,LPCSTR Proj=NULL,LPCSTR Cos=NULL) = 0;

	virtual void UpdateMatrix() = 0;
	virtual void Render(ID3D10EffectTechnique* Technique) = 0; 
	virtual D3DXMATRIX GetWorldMatrix() = 0;
	virtual void SetScale(float size) = 0;
	virtual void SetPosition(D3DXVECTOR3 position) = 0;
	virtual void SetColour(D3DXVECTOR3 colour) = 0;
	virtual D3DXVECTOR3 GetColour() = 0;
	virtual void FacePoint(D3DXVECTOR3 target) = 0;
	virtual void MorphColour(double amount) = 0;
	virtual void MorphBrightness(double amount) = 0;

	virtual ~CLight(){}
};

class CPointLight:public CLight
{
private:
	CModel* m_Light;

	ID3D10EffectVectorVariable* m_LightPos;
	ID3D10EffectVectorVariable* m_LightColour;

	D3DXVECTOR3 m_Colour;
public:
	void SendToShader();
	void LinkToShader(ID3D10Effect* &Effect,LPCSTR Pos,LPCSTR Col,LPCSTR Fac=NULL,LPCSTR View=NULL,LPCSTR Proj=NULL,LPCSTR Cos=NULL);

	void SetColour(D3DXVECTOR3 colour) {m_Colour = colour;}
	D3DXVECTOR3 GetColour() {return m_Colour;}
	void UpdateMatrix() {m_Light->UpdateMatrix();}
	void Render(ID3D10EffectTechnique* Technique) {m_Light->Render(Technique);}
	D3DXMATRIX GetWorldMatrix() {return m_Light->GetWorldMatrix();}
	void SetScale(float size) {m_Light->SetScale(size);}
	void SetPosition(D3DXVECTOR3 position) {m_Light->SetPosition(position);}
	void FacePoint(D3DXVECTOR3 target) {m_Light->FacePoint(target);}
	void MorphColour(double amount);
	void MorphBrightness(double amount);

	CPointLight(ID3D10EffectTechnique* Technique);
	~CPointLight();
};

class CSpotLight:public CLight
{
private:
	CModel* m_Light;

	ID3D10EffectVectorVariable* m_LightPos;
	ID3D10EffectVectorVariable* m_LightFacing;
	ID3D10EffectMatrixVariable* m_LightViewMatrix;
	ID3D10EffectMatrixVariable* m_LightProjMatrix;
	ID3D10EffectScalarVariable* m_LightConeAngle;
	ID3D10EffectVectorVariable* m_LightColour;

	D3DXVECTOR3 m_Colour;
public:
	void SendToShader();
	void LinkToShader(ID3D10Effect* &Effect,LPCSTR Pos,LPCSTR Col,LPCSTR Fac,LPCSTR View,LPCSTR Proj,LPCSTR Cos);

	void SetColour(D3DXVECTOR3 colour) {m_Colour = colour;}
	D3DXVECTOR3 GetColour() {return m_Colour;}
	void UpdateMatrix() {m_Light->UpdateMatrix();}
	void Render(ID3D10EffectTechnique* Technique) {m_Light->Render(Technique);}
	D3DXMATRIX GetWorldMatrix() {return m_Light->GetWorldMatrix();}
	void SetScale(float size) {m_Light->SetScale(size);}
	void SetPosition(D3DXVECTOR3 position) {m_Light->SetPosition(position);}
	void FacePoint(D3DXVECTOR3 target) {m_Light->FacePoint(target);}
	void MorphColour(double amount);
	void MorphBrightness(double amount);

	CSpotLight(ID3D10EffectTechnique* Technique);
	~CSpotLight();
};

#endif